package com.set.app.voicenews.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.set.app.voicenews.dataInfo.DataInfo;

import org.json.JSONException;

import java.io.StringReader;

/**
 * Created by ResonTeng on 2017/1/11.
 */

public class ParserSearchNewsResult {

    public DataInfo.SearchDO parsing(String value) throws JSONException {

        DataInfo.SearchDO mSearchDO = null;

        StringReader jsonSR  = new StringReader(value);

        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();

        JsonReader reader = new JsonReader(jsonSR);
        reader.setLenient(true);
//        Type collectionType = new TypeToken<CateListMix>(){}.getType();
        mSearchDO = gson.fromJson(reader, DataInfo.SearchDO.class);

        return mSearchDO;
    }
}
