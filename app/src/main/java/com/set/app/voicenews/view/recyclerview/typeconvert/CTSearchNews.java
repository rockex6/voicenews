package com.set.app.voicenews.view.recyclerview.typeconvert;


import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.util.prefs.SearchRecordUtil;
import com.set.app.voicenews.view.recyclerview.ViewTypeConstant;
import com.set.app.voicenews.view.recyclerview.ViewTypeInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ResonTeng on 2017/1/10.
 */
public class CTSearchNews {
    private static final String TAG ="CTSearchNews";
    private DataInfo.SearchHotDO mSearchHotDO;
    private List<ViewTypeInfo> mViewTypeList = new ArrayList<ViewTypeInfo>();
    private List<String> keywords;

    public CTSearchNews(List<String> keywords, DataInfo.SearchHotDO mSearchHotDO){
        this.keywords = keywords;
        this.mSearchHotDO = mSearchHotDO;
    }

    public List<ViewTypeInfo> convertType(){
        //熱門搜尋標題
        DataInfo.DescTitleDO mDescTitleDOHT = new DataInfo.DescTitleDO();
        mDescTitleDOHT.describeTitleStr     = "熱門搜尋";
        ViewTypeInfo viewTypeBannerHT       = new ViewTypeInfo();
        viewTypeBannerHT.viewType           = ViewTypeConstant.GeneralCellType.ITEM_DESCRIPTION_TITIE;
        viewTypeBannerHT.dataObject         = mDescTitleDOHT;
        mViewTypeList.add(viewTypeBannerHT);
        //熱門搜尋
        if((mSearchHotDO!=null) && (mSearchHotDO.keywordPop.size()>0)){
            for(int i = 0 ; i<mSearchHotDO.keywordPop.size() ; i++){
                DataInfo.SearchRecordDO mSearchRecordDO = new DataInfo.SearchRecordDO();
                mSearchRecordDO.showRemoveIcon = false;
                mSearchRecordDO.keyword = mSearchHotDO.keywordPop.get(i);
                ViewTypeInfo viewTypeBanner = new ViewTypeInfo();
                viewTypeBanner.viewType     = ViewTypeConstant.GeneralCellType.ITEM_SEARCH_RECORD_CONTENT;
                viewTypeBanner.dataObject   = mSearchRecordDO;
                mViewTypeList.add(viewTypeBanner);
            }
        }

        //搜尋紀錄標題
        DataInfo.DescTitleDO mDescTitleDOT  = new DataInfo.DescTitleDO();
        mDescTitleDOT.describeTitleStr      = "最近搜尋";
        mDescTitleDOT.showDivider           = true;
        ViewTypeInfo viewTypeBannerT        = new ViewTypeInfo();
        viewTypeBannerT.viewType            = ViewTypeConstant.GeneralCellType.ITEM_DESCRIPTION_TITIE;
        viewTypeBannerT.dataObject          = mDescTitleDOT;
        mViewTypeList.add(viewTypeBannerT);
        //搜尋紀錄
        if((keywords!=null) && (keywords.size()>0)){
            for(int i = 0 ; i<keywords.size() ; i++){
                if(i < SearchRecordUtil.RecordItems){
                    DataInfo.SearchRecordDO mSearchRecordDO = new DataInfo.SearchRecordDO();
                    mSearchRecordDO.showRemoveIcon = true;
                    mSearchRecordDO.keyword = keywords.get(i);
                    ViewTypeInfo viewTypeBanner = new ViewTypeInfo();
                    viewTypeBanner.viewType     = ViewTypeConstant.GeneralCellType.ITEM_SEARCH_RECORD_CONTENT;
                    viewTypeBanner.dataObject   = mSearchRecordDO;
                    mViewTypeList.add(viewTypeBanner);
                }
            }
        }

        return mViewTypeList;
    }

}