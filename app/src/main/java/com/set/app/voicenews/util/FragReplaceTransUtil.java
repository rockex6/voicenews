package com.set.app.voicenews.util;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.set.app.voicenews.MainActivity;
import com.set.app.voicenews.R;

/**
 * Created by kennethyeh on 2017/1/4.
 */

public class FragReplaceTransUtil {

    private Context mContext;
    private FragmentManager mFragmentManager;

    public FragReplaceTransUtil(Context context, FragmentManager fragmentManager){
        mContext                = context;
        this.mFragmentManager    = fragmentManager;
    }

    public void replaceContentFragment(Fragment frag, String name){

//        if(mContext instanceof MainActivity) {
//            boolean lockSwitchFragStatus = ((MainActivity) mContext).getLockSwitchFragStatus();
//            if (lockSwitchFragStatus) {
//                return;
//            } else {
//                ((MainActivity) mContext).startSwitchFragLock();
//            }
//        }

        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.replace(R.id.fullFrag, frag, name);
        transaction.addToBackStack(name);
        transaction.commit();

        if(mContext instanceof MainActivity){
            ((MainActivity)mContext).showHideBottomBar(false);
        }
    }

    public void popoutAllFragment(){
        mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
}
