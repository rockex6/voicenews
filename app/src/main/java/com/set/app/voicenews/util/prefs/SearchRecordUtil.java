package com.set.app.voicenews.util.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ResonTeng on 2017/1/10.
 */
public class SearchRecordUtil {
    private String TAG = "SearchRecordUtil";
    private Context mContext = null;
    private Gson gson;

    private SharedPreferences prefs = null;
    public static String PREFS_NAME = "SearchNews";
    public static String PREFS_KEY  = "SearchHistory";
    public static final int RecordItems = 5; //搜尋紀錄最大顯示筆數

    public static synchronized SearchRecordUtil getInstance(Context context){
        return new SearchRecordUtil(context);
    }

    private SearchRecordUtil(Context context){
        mContext = context;
        prefs    = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        gson     = new Gson();
    }

    /**
     * 取得preference的搜尋keyword紀錄清單
     * **/
    public List<String> getRecordsList(){
        List<String> keywords = new ArrayList<String>();

        String getJson = prefs.getString(PREFS_KEY, null);
        if (getJson!=null){
            Type type = new TypeToken<ArrayList<String>>() {}.getType();
            keywords = gson.fromJson(getJson, type);
        }
        return keywords;
    }

    /**
     * 搜尋keyword寫入preference
     * 重複搜尋關鍵字，更新為第一筆
     * **/
    public void putKeyword(String keyword){
        List<String> keywords = getRecordsList();

        if(keywords!=null && keywords.size()>0) {
            for (int i = 0; keywords.size() > i; i++) {
                if (keyword.trim().equals(keywords.get(i))) {
                    keywords.remove(i);
                }
            }
        }

        keywords.add(0, keyword.trim());
        String putJson = gson.toJson(keywords);
        prefs.edit().putString(PREFS_KEY, putJson).commit();
    }
    //回傳更新後的紀錄清單
    public List<String> putKeyword2RecordsList(String keyword, List<String> keywords){
        if(keywords!=null && keywords.size()>0) {
            for (int i = 0; keywords.size() > i; i++) {
                if (keyword.trim().equals(keywords.get(i))) {
                    keywords.remove(i);
                }
            }
        }

        keywords.add(0, keyword.trim());
        String putJson = gson.toJson(keywords);
        prefs.edit().putString(PREFS_KEY, putJson).commit();

        return keywords;
    }

    /**
     * 刪除keyword寫入preference
     * @param nextRecord : 回傳是否有下一筆紀錄
     * **/
    private String nextRecord = null;
    public void removeKeyword(String keyword){
        List<String> keywords = getRecordsList();

        List<String> compareList = new ArrayList<String>();
        for(int i=0; i<keywords.size(); i++){
            if (!keywords.get(i).equals(keyword)){
                compareList.add(keywords.get(i));
            }else{
                nextRecord = null;
                if(keywords.size() > RecordItems){ //確認是否紀錄下一筆搜尋紀錄
                    nextRecord = keywords.get(RecordItems);
                }
            }
        }

        String putJson = gson.toJson(compareList);
        prefs.edit().putString(PREFS_KEY, putJson).commit();
    }
    public synchronized String getNextRecord(){
        return nextRecord;
    }
}
