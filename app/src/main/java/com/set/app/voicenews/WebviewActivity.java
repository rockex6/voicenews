package com.set.app.voicenews;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.set.app.voicenews.constant.GlobalConstant;
import com.set.app.voicenews.util.DebugLog;

/**
 * Created by kennethyeh on 2017/1/4.
 */

public class WebviewActivity extends AppCompatActivity {

    private static final String TAG ="WebviewActivity";
    private Context mContext;
    private View statusBGView;
    private AppBarLayout mAppBarLayout;
    private Toolbar toolbar;
    private ProgressBar mProgressBar;
    private WebView wvWeb = null;
    private boolean useViewPort = true;

    public static final String Para_key_Title = "Title" ;

    public static final String Para_key_Type    = "Type" ;
    public static final int TYPE_MORMAL         = 0;
    public static final int TYPE_FB_MESSAGE     = 1;

    private int type ;
    private String title = "";
    private String openUrl = "";

    private enum ToolbarState {
        EXPANDED,
        COLLAPSED,
        IDLE
    }
    private ToolbarState mToolbarState=ToolbarState.EXPANDED;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);

        setContentView(R.layout.activity_webview);
        mContext = this;
        statusBGView    = (View) findViewById(R.id.statusBGView);
        mAppBarLayout   = (AppBarLayout) findViewById(R.id.appBarLayout);
        toolbar         = (Toolbar) findViewById(R.id.toolbar);

        type    = getIntent().getIntExtra(Para_key_Type, TYPE_MORMAL);
        title   = getIntent().getStringExtra(Para_key_Title);
        openUrl = this.getIntent().getStringExtra("url");
        DebugLog.d(TAG, "type:"+type);
        DebugLog.d(TAG, "title:"+title);
        DebugLog.d(TAG, "openUrl:"+openUrl);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setToolBarContent();
        initView();

        if (Build.VERSION.SDK_INT >= 21) {
            Window w = getWindow();
            // Set the status bar to dark-semi-transparentish
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
//                       WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // status bar 可自定半透明顏色solution
            w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            w.setStatusBarColor(ContextCompat.getColor(this, R.color.statusBarTransparent));
            // Fixes statusbar covers toolbar issue
            int statusBarHeight = getStatusBarHeight();
            statusBGView.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams paramsStatusBG = (RelativeLayout.LayoutParams) statusBGView.getLayoutParams();
            paramsStatusBG.height = (GlobalConstant.Status_Bar_Height==0)?(statusBarHeight):(GlobalConstant.Status_Bar_Height);
            statusBGView.setLayoutParams(paramsStatusBG);

            mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    if (verticalOffset == 0) {
                        mToolbarState=ToolbarState.EXPANDED;
//                    DebugLog.d(TAG, ""+mToolbarState);
                    } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                        mToolbarState=ToolbarState.COLLAPSED;
//                    DebugLog.d(TAG, ""+mToolbarState);
                    } else {
                        mToolbarState=ToolbarState.IDLE;
//                    DebugLog.d(TAG, ""+mToolbarState);
                    }
                }
            });
        }
    }

    private void setToolBarContent(){
        if( (title !=null) && (!title.trim().equalsIgnoreCase("")) ){
            getSupportActionBar().setTitle(title);
        }
    }

    // A method to find height of the status bar
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void initView() {

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mProgressBar.setMax(100);
        mProgressBar.setVisibility(View.INVISIBLE);

        wvWeb = (WebView) this.findViewById(R.id.wvWeb);

        if (Build.VERSION.SDK_INT >= 21) {
            wvWeb.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            CookieManager.getInstance().setAcceptThirdPartyCookies(wvWeb, true);
        }

        wvWeb.getSettings().setJavaScriptEnabled(true);
        if(useViewPort){
            wvWeb.getSettings().setUseWideViewPort(true);
            wvWeb.getSettings().setLoadWithOverviewMode(true);
        }

        wvWeb.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                mProgressBar.setProgress(0);
                view.loadUrl(url);
                mProgressBar.setVisibility(View.VISIBLE);
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url) {
//                DebugLog.i(TAG, "onPageFinished url:"+url);
                if(url.startsWith("https://m.facebook.com/plugins/close_popup.php")){
//                    DebugLog.d(TAG, "startsWith url:"+url);
                    fbLoginFinish();
                    return;
                }
                super.onPageFinished(view, url);
            }

        });
        wvWeb.setWebChromeClient(chromeClient);

        mProgressBar.setProgress(0);
        wvWeb.loadUrl(openUrl);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private boolean finishLoginFBFlag = false;
    private synchronized void fbLoginFinish(){
        if(finishLoginFBFlag){return;}// 避免重覆callback
        finishLoginFBFlag = true;
        wvWeb.loadUrl(openUrl);
    }

    private WebChromeClient chromeClient = new WebChromeClient() {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            setValue(newProgress);
            super.onProgressChanged(view, newProgress);
        }

    };

    public void setValue(int progress) {
//		Log.d(TAG, "progress:" + progress);
        mProgressBar.setProgress(progress);
        if(progress==100){
            mProgressBar.setVisibility(View.INVISIBLE);
            mProgressBar.setProgress(0);
        }
    }

    public void finishMenuRequest(){
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_content_webview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_share){
            share();
            return true;
        }
        if(id == android.R.id.home){
            WebviewActivity.this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item); // important line
    }

    private void share(){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, title);
        String AppNameDisplay = mContext.getResources().getString(R.string.app_name);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, title + " | "+AppNameDisplay+" | " + openUrl);
        mContext.startActivity(Intent.createChooser(sharingIntent, "分享連結"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(wvWeb!=null){
            DebugLog.d(TAG, "onDestroy");
            wvWeb.onPause();
            wvWeb.destroy();
            wvWeb = null;
        }
    }

    @Override
    public void onBackPressed() {
        if(type==TYPE_FB_MESSAGE){
            WebviewActivity.this.finish();
            return;
        }
        if (wvWeb.canGoBack()) {
            wvWeb.goBack();
            mAppBarLayout.setExpanded(true);
        } else {
            super.onBackPressed();
        }
    }
}

