package com.set.app.voicenews.util.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;

import com.set.app.voicenews.R;

/**
 * Created by kennethyeh on 2017/1/9.
 */

public class SetNewsSourceUtil {

    private String TAG = "SetReqTokenUtil";

    private Context mContext = null;

    private SharedPreferences prefs                     = null;
    private static String PREFS_NAME 	                = "NewsSourcePrefInfo";
//    private static final String PREFS_KEY_Token         = "KeyToken";
    private static final String PREFS_KEY_Edit         = "KeyEdit"; // 紀錄是否有異動狀態

    public static synchronized SetNewsSourceUtil getInstance(Context context){
        return new SetNewsSourceUtil(context);
    }

    private SetNewsSourceUtil(Context context){
        mContext    = context;
        prefs       = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public synchronized void setNewsSourcePref(String source, boolean like){
        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(source, like);
        editor.commit();
        setPrefEdit(true);
    }

    public synchronized boolean getNewsSourcePref(String source){
        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getBoolean(source, true);
    }

    /**
     * 設定異動狀態
     * **/
    public synchronized void setPrefEdit(boolean edit){
        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREFS_KEY_Edit, edit);
        editor.commit();
    }

    /**
     * 取得異動狀態
     * **/
    public synchronized boolean getPrefEdit(){
        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        return prefs.getBoolean(PREFS_KEY_Edit, false);
    }


    public synchronized void toggleNewsSourcePref(String source, ImageView checkIV){
        if(getNewsSourcePref(source)){
            setNewsSourcePref(source, false);
            checkIV.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_subs_add));
        }else{
            setNewsSourcePref(source, true);
            checkIV.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_subs_checked));
        }
    }

}
