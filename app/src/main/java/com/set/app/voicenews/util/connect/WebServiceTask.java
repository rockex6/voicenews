package com.set.app.voicenews.util.connect;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import com.set.app.voicenews.requestcontrol.ApiRequestControl;
import com.set.app.voicenews.requestcontrol.RequestPara;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class WebServiceTask extends AsyncTask<Void, Integer, Integer> {
    private Handler requestHandler;
    private RequestPara requrestPara;

    public WebServiceTask(Handler requestHandler, RequestPara requrestPara){

        this.requestHandler	= requestHandler;
        this.requrestPara 	= requrestPara;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Integer doInBackground(Void... params) {

        int httpStatus=0;
        int requestMethod = requrestPara.httpMethod;

        if(requestMethod == RequestPara.HTTP_METHOD_GET ){
            HttpGetUtil connectGet = new HttpGetUtil();
            requrestPara = connectGet.getHttpStatus(requrestPara);
        }
        if(requestMethod == RequestPara.HTTP_METHOD_POST ){
            HttpPostUtil connectPost = new HttpPostUtil();
            requrestPara = connectPost.getHttpStatus(requrestPara);
        }

        if(requestMethod == RequestPara.HTTP_METHOD_POST_JSON ){
            HttpPostJSONUtil mHttpPostJSONUtil = new HttpPostJSONUtil();
            requrestPara = mHttpPostJSONUtil.getHttpStatus(requrestPara);
        }
        return httpStatus;
    }

    @Override
    protected void onPostExecute(Integer status) {

        Message msg = Message.obtain(); // Creates an new Message instance
        msg.what = ApiRequestControl.PARSE_RESPONSE;
        msg.obj = requrestPara;// Put the ArrayList into Message, into "obj" field.
        msg.setTarget(requestHandler); // Set the Handler
        msg.sendToTarget(); //Send the message
    }
}