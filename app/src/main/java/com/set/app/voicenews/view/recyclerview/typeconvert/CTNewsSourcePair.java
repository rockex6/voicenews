package com.set.app.voicenews.view.recyclerview.typeconvert;

import android.content.Context;

import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.view.recyclerview.ViewTypeConstant;
import com.set.app.voicenews.view.recyclerview.ViewTypeInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kennethyeh on 2017/1/9.
 */

public class CTNewsSourcePair {
    private final String TAG = "CTCateHomeNews";
    private Context mContext;
    private int cellGridCount   = 2;


    public CTNewsSourcePair(Context mContext){

        this.mContext            = mContext;
    }

    public List<ViewTypeInfo> convertSourcePairList(List<DataInfo.NewsSourceInfo> list){
        List<ViewTypeInfo> mViewTypeList = new ArrayList<ViewTypeInfo>();



        List<DataInfo.NewsSourceInfo> gridList = list;

        int quotient    = gridList.size()/cellGridCount; // 商
        int remainder   = gridList.size()%cellGridCount; // 餘
        int rowCount   = (remainder<1)?(quotient):(quotient+1); // list rowCount
        for(int i=0; i<rowCount ;i++){
            List<DataInfo.NewsSourceInfo> cellGridList = new ArrayList<>();
            for(int j = 0; j<cellGridCount ; j++){
                int getPosition = cellGridCount*i+j;
                if(getPosition<gridList.size()){
                    DataInfo.NewsSourceInfo source = gridList.get(getPosition);
                    cellGridList.add(source);
                }
            }
            ViewTypeInfo viewTypeVideoLives = new ViewTypeInfo();
            viewTypeVideoLives.viewType = ViewTypeConstant.GeneralCellType.ITEM_GRID_PAIR_NEWS_SOURCE;
            viewTypeVideoLives.dataObject = cellGridList;
            mViewTypeList.add(viewTypeVideoLives);
        }
        return mViewTypeList;
    }
}
