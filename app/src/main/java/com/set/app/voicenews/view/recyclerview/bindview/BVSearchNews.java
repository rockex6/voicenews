package com.set.app.voicenews.view.recyclerview.bindview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;

import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.frag.cate.FragCateSearchNews;
import com.set.app.voicenews.util.prefs.SearchRecordUtil;
import com.set.app.voicenews.view.recyclerview.ViewTypeConstant;
import com.set.app.voicenews.view.recyclerview.ViewTypeInfo;
import com.set.app.voicenews.view.recyclerview.createview.CVGeneralList;

import java.util.List;

/**
 * Created by ResonTeng on 2017/1/10.
 */
public class BVSearchNews {

    private static final String TAG ="BVSearchNews";

    private Context mContext;
    private List<ViewTypeInfo> viewTypeList;
    private SearchView mSearchView;

    public BVSearchNews(Context mContext, List<ViewTypeInfo> viewTypeList, SearchView mSearchView){
        this.mContext       = mContext;
        this.viewTypeList   = viewTypeList;
        this.mSearchView    = mSearchView;
    }

    public void setViewHolder(RecyclerView.ViewHolder viewHolder, int position){
        ViewTypeInfo viewTypeInfo = viewTypeList.get(position);
        int viewType = viewHolder.getItemViewType();
        switch (viewType) {

            case ViewTypeConstant.GeneralCellType.ITEM_DESCRIPTION_TITIE:
                CVGeneralList.HolderDescriptionTitle historyTitleViewHolder = (CVGeneralList.HolderDescriptionTitle) viewHolder;
                DataInfo.DescTitleDO mDescTitleDO = (DataInfo.DescTitleDO) viewTypeInfo.dataObject;
                historyTitleViewHolder.describeTitle.setText(mDescTitleDO.describeTitleStr);
                if(mDescTitleDO.showDivider){
                    historyTitleViewHolder.divider.setVisibility(View.VISIBLE);
                }else{
                    historyTitleViewHolder.divider.setVisibility(View.GONE);
                }
                break;

            case ViewTypeConstant.GeneralCellType.ITEM_SEARCH_RECORD_CONTENT:
                CVGeneralList.HolderSearchRecordContent historyViewHolder = (CVGeneralList.HolderSearchRecordContent) viewHolder;
                DataInfo.SearchRecordDO mSearchRecordDO = (DataInfo.SearchRecordDO) viewTypeInfo.dataObject;
                String recordName = mSearchRecordDO.keyword;
                historyViewHolder.recordNameTV.setText(recordName);
                historyViewHolder.recordItemRL.setTag(recordName);
                historyViewHolder.recordItemRL.setOnClickListener(clickKeywordSearchListener);
                historyViewHolder.recordCleanIV.setTag(recordName);
                historyViewHolder.recordCleanIV.setOnClickListener(recordCleanListener);
                if(mSearchRecordDO.showRemoveIcon){
                    historyViewHolder.recordCleanIV.setVisibility(View.VISIBLE);
                }else{
                    historyViewHolder.recordCleanIV.setVisibility(View.GONE);
                }
                break;
        }
    }

    private View.OnClickListener clickKeywordSearchListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            mSearchView.setQuery(String.valueOf(v.getTag()), true);
        }
    };

    private View.OnClickListener recordCleanListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            String recordName = v.getTag().toString();
            cleanRecord(recordName);
            FragCateSearchNews.removeRecord(recordName, nextRecord);
        }
    };

    private String nextRecord = null; //判斷是否有下一筆紀錄
    private void cleanRecord(String key){
        SearchRecordUtil mSearchRecordUtil = SearchRecordUtil.getInstance(mContext);
        mSearchRecordUtil.removeKeyword(key);
        nextRecord = mSearchRecordUtil.getNextRecord();
    }
}

