package com.set.app.voicenews.view.recyclerview;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class ViewTypeInfo {
    public int viewType;
    public Object dataObject;

    public ViewTypeInfo() {
    }

    public ViewTypeInfo(int viewType, Object dataObject) {
        this.viewType = viewType;
        this.dataObject = dataObject;
    }

}
