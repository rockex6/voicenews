package com.set.app.voicenews.util.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.set.app.voicenews.dataInfo.DataInfo;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class SetReqTokenUtil {

    private String TAG = "SetReqTokenUtil";

    private Context mContext = null;

    private SharedPreferences prefs                     = null;
    private static String PREFS_NAME 	                = "TokenInfo";
    private static final String PREFS_KEY_Token         = "KeyToken";
    private static final String PREFS_KEY_Expiration    = "KeyExpiration";

    public static synchronized SetReqTokenUtil getInstance(Context context){
        return new SetReqTokenUtil(context);
    }

    private SetReqTokenUtil(Context context){
        mContext    = context;
        prefs       = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public synchronized void setTokenInfo(DataInfo.TokenInfo tokenInfo){
        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREFS_KEY_Token, tokenInfo.token);
        editor.putString(PREFS_KEY_Expiration, tokenInfo.expiration);
        editor.commit();
    }

    public synchronized DataInfo.TokenInfo getReqTokenInfo(){
        DataInfo.TokenInfo mTokenInfo = new DataInfo.TokenInfo();
        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        mTokenInfo.token      = prefs.getString(PREFS_KEY_Token, "");
        mTokenInfo.expiration = prefs.getString(PREFS_KEY_Expiration, "");
        return mTokenInfo;
    }
}
