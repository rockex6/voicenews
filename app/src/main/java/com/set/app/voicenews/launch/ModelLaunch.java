package com.set.app.voicenews.launch;

import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.set.app.voicenews.MainActivity;
import com.set.app.voicenews.R;
import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.frag.launch.FragLaunchPhoto;
import com.set.app.voicenews.requestcontrol.ApiRequestControl;
import com.set.app.voicenews.util.DebugLog;
import com.set.app.voicenews.util.GetNewReqTokenUtil;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class ModelLaunch {

    private static final String TAG         = "ModelLaunch";
    private MainActivity mContext      = null;
    private FragmentManager fragmentManager;
    private ApiRequestControl mApiRequestControl;

    private RelativeLayout mRefreshRL;
    private TextView mAlertTV;
    private ProgressBar mProgressBar;

    private FragLaunchPhoto mFragLaunchPhoto            = null;
    private Handler mLaunchPhotoHandler = new Handler();
    private int splashsTime = 2500;

    private LaunchFinishListener mLaunchFinishListener = null;
    public interface LaunchFinishListener{
        void launchDone();
    }


    public ModelLaunch(MainActivity context, LaunchFinishListener launchFinishListener){
        this.mContext           = (MainActivity) context;
        fragmentManager         = ((MainActivity)mContext).fragmentManager;
        mLaunchFinishListener   = launchFinishListener;
        mRefreshRL              = (RelativeLayout) mContext.findViewById(R.id.refreshRL);
        mAlertTV                = (TextView) mContext.findViewById(R.id.alertTV);
        mProgressBar            = (ProgressBar) mContext.findViewById(R.id.loadingProgressBar);
    }

    /** Launch流程
     * **/
    public void starLaunch(){
        DebugLog.i(TAG, "@@ starLaunch");

        mFragLaunchPhoto = FragLaunchPhoto.newInstance();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fullFrag, mFragLaunchPhoto, FragLaunchPhoto.FRAG_TRANS_NAME);
        transaction.commit();
        mLaunchPhotoHandler.postDelayed(new Runnable(){

            @Override
            public void run() {
                DebugLog.i(TAG, "@@ finishLaunchPhoto");
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.remove(mFragLaunchPhoto).commit();
                ((MainActivity)mContext).showHideBottomBar(true);
            }
        }, splashsTime);
        dataRequest();
    }

    private void dataRequest(){
        mProgressBar.setVisibility(View.VISIBLE);
        getToken();
    }

    private void getToken(){
        GetNewReqTokenUtil mGetNewReqTokenUtil = new GetNewReqTokenUtil(mContext);
        mGetNewReqTokenUtil.setGetTokenListener(new GetNewReqTokenUtil.GetTokenListener(){
            @Override
            public void haveToken(DataInfo.TokenInfo token) {
                DebugLog.d(TAG, "mtoken:"+token.token);
                DebugLog.d(TAG, "expiration:"+token.expiration);
                finishLaunch();
            }
            @Override
            public void connectErr(String error) {
                showAlert(error);
            }
        });
        mGetNewReqTokenUtil.tokenRequest();
    }


    public void showAlert(String alert){
            mProgressBar.setVisibility(View.GONE);
            mAlertTV.setText(alert);
            mRefreshRL.setVisibility(View.VISIBLE);
            mRefreshRL.setOnClickListener(retryRequestListener);
            return;

    }

    private View.OnClickListener retryRequestListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            mRefreshRL.setVisibility(View.GONE);
            dataRequest();
        }
    };

    private void finishLaunch(){
        mProgressBar.setVisibility(View.GONE);
        if(mLaunchFinishListener!=null){
            DebugLog.i(TAG, "@@ finishLaunch");
            DebugLog.i(TAG, "------------------");
            mLaunchFinishListener.launchDone();
        }
    }
}
