package com.set.app.voicenews;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.set.app.voicenews.constant.GlobalConstant;
import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.frag.FragBlankWebview;
import com.set.app.voicenews.util.DebugLog;

import java.util.HashMap;
import java.util.List;

/**
 * Created by kennethyeh on 2017/1/20.
 */

public class WebviewPagerActivity extends AppCompatActivity {

    private static final String TAG ="WebviewPagerActivity";
    private Context mContext;
    private FragmentManager fragmentManager;
    private View statusBGView;
    private AppBarLayout mAppBarLayout;
    private Toolbar toolbar;
    private ViewPager mPager;

    private String title = "";

    public static final String Para_key_Title               = "Title" ;
    public static final String Para_key_NewsList            = "NewsList" ;
    public static final String Para_key_Position_in_List    = "Position" ;
    private List<Object> mNewsList ;
    private int newsPosition ;

    private HashMap<Integer, Fragment> mFragMap = new HashMap<Integer, Fragment>();

    private enum ToolbarState {
        EXPANDED,
        COLLAPSED,
        IDLE
    }
    private ToolbarState mToolbarState = ToolbarState.EXPANDED;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);

        setContentView(R.layout.activity_webview_pager);

        mContext        = this;
        fragmentManager = getSupportFragmentManager();
        statusBGView    = (View) findViewById(R.id.statusBGView);
        mAppBarLayout   = (AppBarLayout) findViewById(R.id.appBarLayout);
        toolbar         = (Toolbar) findViewById(R.id.toolbar);

        title           = getIntent().getStringExtra(Para_key_Title);
        mNewsList       = (List<Object>) getIntent().getSerializableExtra(Para_key_NewsList);
        newsPosition    = getIntent().getIntExtra(Para_key_Position_in_List, 0);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setToolBarContent();

        mPager          = (ViewPager) findViewById(R.id.pager);
        setPager(newsPosition);
        DebugLog.d(TAG,"newsPosition:"+newsPosition);
        DebugLog.d(TAG,"news size:"+mNewsList.size());
//        initView();

        if (Build.VERSION.SDK_INT >= 21) {
            Window w = getWindow();
            // Set the status bar to dark-semi-transparentish
//            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
//                       WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // status bar 可自定半透明顏色solution
            w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            w.setStatusBarColor(ContextCompat.getColor(this, R.color.statusBarTransparent));
            // Fixes statusbar covers toolbar issue
            int statusBarHeight = getStatusBarHeight();
            statusBGView.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams paramsStatusBG = (RelativeLayout.LayoutParams) statusBGView.getLayoutParams();
            paramsStatusBG.height = (GlobalConstant.Status_Bar_Height==0)?(statusBarHeight):(GlobalConstant.Status_Bar_Height);
            statusBGView.setLayoutParams(paramsStatusBG);

            mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                @Override
                public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                    if (verticalOffset == 0) {
                        mToolbarState= ToolbarState.EXPANDED;
//                    DebugLog.d(TAG, ""+mToolbarState);
                    } else if (Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange()) {
                        mToolbarState= ToolbarState.COLLAPSED;
//                    DebugLog.d(TAG, ""+mToolbarState);
                    } else {
                        mToolbarState= ToolbarState.IDLE;
//                    DebugLog.d(TAG, ""+mToolbarState);
                    }
                }
            });
        }
    }

    private void setToolBarContent(){
        if( (title !=null) && (!title.trim().equalsIgnoreCase("")) ){
            getSupportActionBar().setTitle(title);
        }
    }

    // A method to find height of the status bar
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void setPager(int startPagePosition){
        Adapter adapter = new Adapter(fragmentManager, mNewsList);
        mPager.setAdapter(adapter);
        mPager.addOnPageChangeListener(pageChangeListener);
//        startPagePosition = 2; // for test pos
        mPager.setCurrentItem(startPagePosition);
        mPager.setOffscreenPageLimit(1);
    }

    private ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener(){
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
        @Override
        public void onPageScrollStateChanged(int state) {}
        @Override
        public void onPageSelected(int position) {
            DebugLog.d(TAG,"onPageSelected:"+position);
            newsPosition = position;
        }
    };

    class Adapter extends FragmentStatePagerAdapter {

        private int originalSize=0;
        private List<Object> mNewsList ;

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public Adapter(FragmentManager fm, List<Object> mNewsList) {
            super(fm);
            this.originalSize 	= mNewsList.size();
            this.mNewsList 	= mNewsList;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag   = null;
//            frag = FragmentUnderConstruction.newInstance("");
            frag=FragBlankWebview.newInstance(getURL(position));
            mFragMap.put(position, frag);
            return frag;
        }

        @Override
        public int getCount() {
            return originalSize;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            mFragMap.remove(position);
        }

        private String getURL(int position){
            String url="";
            Object newsDate = mNewsList.get(position);
            if(newsDate instanceof DataInfo.NewsDO){
                url = ((DataInfo.NewsDO) newsDate).url;
            }
            if(newsDate instanceof DataInfo.PttNewsDO ){
                url = ((DataInfo.PttNewsDO) newsDate).url;
            }
            return url;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_content_webview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_share){
            share();
            return true;
        }
        if(id == android.R.id.home){
            WebviewPagerActivity.this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item); // important line
    }

    private void share(){
        String url="";
        String title = "";
        Object newsDate = mNewsList.get(newsPosition);
        if(newsDate instanceof DataInfo.NewsDO){
            url = ((DataInfo.NewsDO) newsDate).url;
            title = ((DataInfo.NewsDO) newsDate).title;
        }
        if(newsDate instanceof DataInfo.PttNewsDO ){
            url = ((DataInfo.PttNewsDO) newsDate).url;
            title = ((DataInfo.PttNewsDO) newsDate).title;
        }

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, title);
        String AppNameDisplay = mContext.getResources().getString(R.string.app_name);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, title + " | "+AppNameDisplay+" | " + url);
        mContext.startActivity(Intent.createChooser(sharingIntent, "分享連結"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DebugLog.d(TAG,"onBackPressed");
        FragBlankWebview frag = (FragBlankWebview) mFragMap.get(newsPosition);
        frag.onBackPressed();
    }

}
