package com.set.app.voicenews.frag;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.set.app.voicenews.MainActivity;
import com.set.app.voicenews.R;
import com.set.app.voicenews.constant.APIConstant;
import com.set.app.voicenews.constant.GlobalConstant;
import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.frag.base.BaseToolbarRecyclerFragment;
import com.set.app.voicenews.requestcontrol.ApiRequestControl;
import com.set.app.voicenews.requestcontrol.RequestPara;
import com.set.app.voicenews.requestcontrol.RequestParseType;
import com.set.app.voicenews.util.DebugLog;
import com.set.app.voicenews.util.GetNewReqTokenUtil;
import com.set.app.voicenews.util.connect.ConnectionConstant;
import com.set.app.voicenews.util.prefs.SetReqTokenUtil;
import com.set.app.voicenews.view.recyclerview.ViewTypeInfo;
import com.set.app.voicenews.view.recyclerview.bindview.BVNewsSourcePair;
import com.set.app.voicenews.view.recyclerview.createview.CVGeneralList;
import com.set.app.voicenews.view.recyclerview.typeconvert.CTNewsSourcePair;

import java.net.HttpURLConnection;
import java.util.List;

/**
 * Created by kennethyeh on 2017/1/9.
 */

public class FragEditNewsSource extends BaseToolbarRecyclerFragment {

    private static final String TAG = "FragEditNewsSource";
    public static final String FRAG_TRANS_NAME 	= "FragEditNewsSource";

    private Context mContext;

    private boolean loadingFlag = false;
    private ApiRequestControl mApiRequestControl;

    private int nowListLoadStatus   = LIST_INIT;

    private RecyclerViewAdapter mRecyclerViewAdapter;

    public static FragEditNewsSource newInstance() {

        FragEditNewsSource frag = new FragEditNewsSource();
        Bundle b = new Bundle();
        frag.setArguments(b);
        return frag;
    }

    public FragEditNewsSource(){}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext  = context;
    }

    @Override
    protected void init() {
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(mContext.getResources().getString(R.string.cate_news_edit));

        currentLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.news_edit_bg_color));
        dataRequest();
    }

    @Override
    protected void setStatusBGView(View statusBGView) {
        if (Build.VERSION.SDK_INT >= 21) {
            statusBGView.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams paramsStatusBG = (RelativeLayout.LayoutParams) statusBGView.getLayoutParams();
            paramsStatusBG.height = GlobalConstant.Status_Bar_Height;
            statusBGView.setLayoutParams(paramsStatusBG);
        }
    }

    @Override
    protected void resumeFrag() {

    }

    private synchronized void dataRequest(){
        if(loadingFlag){//資料截取中
            return;
        }
        RequestPara mRequrestPara       = new RequestPara();
        mRequrestPara.httpMethod 	    = RequestPara.HTTP_METHOD_GET;
        mRequrestPara.parseType   	    = RequestParseType.ParseType.NewsSource;
        mRequrestPara.requestURL 	    = getRequestURL();

        mApiRequestControl 			= new ApiRequestControl(requestCallBack, mRequrestPara, true);
        mApiRequestControl.startRequest();
        mRefreshRL.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
        if(nowListLoadStatus==LIST_INIT){
            mProgressBar.setVisibility(View.VISIBLE);
        }
        loadingFlag = true;
    }

    private String getRequestURL(){
        String url      = "";
        SetReqTokenUtil mSetReqTokenUtil = SetReqTokenUtil.getInstance(mContext);
        String token  = mSetReqTokenUtil.getReqTokenInfo().token;
        url = String.format(APIConstant.API_Get_News_Source, token);
        DebugLog.d(TAG, "url:" + url);
        return url;
    }

    private ApiRequestControl.CallBack requestCallBack = new ApiRequestControl.CallBack() {

        @Override
        public void doCallBack(Object obj) {
            RequestPara requrestPara = (RequestPara) obj;
            RequestParseType.ParseType parseType = requrestPara.parseType;

            DebugLog.d(TAG, "NewsSource!");
            DebugLog.d(TAG, "nowListLoadStatus:"+nowListLoadStatus);
            int httpStatus = requrestPara.httpStatus;
            DebugLog.d(TAG, "httpStatus:"+httpStatus);
//                    DebugLog.d(TAG, "responseStrContent:"+requrestPara.responseStrContent);
            if(ConnectionConstant.checkConnectError(httpStatus)){
                String alertStr = "連線異常 請確認網路狀態請稍後再試！";
                showAlert(alertStr);
                finishResponse();
                return;
            }
            if(httpStatus!= HttpURLConnection.HTTP_OK){
                String alertStr = "Server連線異常請稍後再試！";
                showAlert(alertStr);
                finishResponse();
                return;
            }
            if(requrestPara.responseObject==null && requrestPara.responseStrContent.indexOf("token expired")>-1){
                DebugLog.d(TAG, "responseStrContent:"+requrestPara.responseStrContent);
                getTokenAgain();
                return;
            }
            DataInfo.NewsSource mNewsSource = (DataInfo.NewsSource) requrestPara.responseObject;
            CTNewsSourcePair mCTNewsSourcePair = new CTNewsSourcePair(mContext);
            List<ViewTypeInfo> mViewTypeList = mCTNewsSourcePair.convertSourcePairList(mNewsSource.data);
            setAdapter(mViewTypeList);
            finishResponse();
        }
    };

    private void getTokenAgain(){
        DebugLog.i(TAG,"token expired getTokenAgain");
        GetNewReqTokenUtil mGetNewReqTokenUtil = new GetNewReqTokenUtil(mContext);
        mGetNewReqTokenUtil.setGetTokenListener(new GetNewReqTokenUtil.GetTokenListener(){
            @Override
            public void haveToken(DataInfo.TokenInfo token) {
                loadingFlag = false;
//                testOldToken="";
                dataRequest();
            }
            @Override
            public void connectErr(String error) {
                showAlert(error);
                finishResponse();
            }
        });
        mGetNewReqTokenUtil.tokenRequest();
    }

    public void showAlert(String alert){
        if(nowListLoadStatus == LIST_INIT){
            mAlertTV.setText(alert);
            mRefreshRL.setVisibility(View.VISIBLE);
            return;
        }
        if(nowListLoadStatus == LIST_REFRESH){// Refresh異常
//            Toast.makeText(mContext, alert, Toast.LENGTH_SHORT).show();
            try {
                Snackbar.make(mRecyclerView, alert, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }catch (Exception e){}
            return;
        }

    }

    private void finishResponse(){
        mProgressBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
        loadingFlag = false;
        mProgressBar.setVisibility(View.GONE);
        DebugLog.d(TAG, "-----------");
    }

    private void setAdapter(List<ViewTypeInfo> viewTypeList){
        if(mRecyclerViewAdapter!=null){
            mRecyclerViewAdapter.removeAll();
        }
        mRecyclerViewAdapter = new RecyclerViewAdapter(viewTypeList);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
    }

    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<ViewTypeInfo> viewTypeList;
        private CVGeneralList mCVGeneralList;
        private BVNewsSourcePair mBVNewsSource;

        public RecyclerViewAdapter(List<ViewTypeInfo> viewTypeList){
            this.viewTypeList   = viewTypeList;
            mCVGeneralList      = new CVGeneralList();
            mBVNewsSource     = new BVNewsSourcePair(mContext, viewTypeList);
        }

        @Override
        public int getItemCount() {
            return viewTypeList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return viewTypeList.get(position).viewType;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = mCVGeneralList.getViewHolder(parent, viewType);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            mBVNewsSource.setViewHolder(viewHolder, position);
        }

        public void removeAll(){
            viewTypeList.removeAll(viewTypeList);
            mRecyclerViewAdapter.notifyDataSetChanged();
        }

    }

    @Override
    protected void setRetryRequestListener(RelativeLayout mRefreshRL) {
        mRefreshRL.setOnClickListener(retryRequestListener);
    }
    private View.OnClickListener retryRequestListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            mRefreshRL.setVisibility(View.GONE);
            init();
        }
    };

    @Override
    protected void setOnRefreshListener(SwipeRefreshLayout mSwipeRefreshLayout) {
        mSwipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.black, R.color.black, R.color.black);
    }
    private SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener(){
        @Override
        public void onRefresh() {
            if(loadingFlag){
                return;// do nothing
            }
            DebugLog.d(TAG, "onRefresh");
            nowListLoadStatus = LIST_REFRESH;
            dataRequest();
        }
    };

    @Override
    protected void setOnScrollListener(RecyclerView mRecyclerView) {

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        DebugLog.d(TAG, "onCreateOptionsMenu");
        mToolbar.getMenu().clear();
        inflater.inflate(R.menu.menu_edit_news_source, mToolbar.getMenu());

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id ==  R.id.action_edit_cancel){
                ((MainActivity)mContext).onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item); // important line
    }
}
