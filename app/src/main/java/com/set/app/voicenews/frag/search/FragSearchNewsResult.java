package com.set.app.voicenews.frag.search;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.set.app.voicenews.MainActivity;
import com.set.app.voicenews.R;
import com.set.app.voicenews.constant.APIConstant;
import com.set.app.voicenews.constant.GlobalConstant;
import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.requestcontrol.ApiRequestControl;
import com.set.app.voicenews.requestcontrol.RequestPara;
import com.set.app.voicenews.requestcontrol.RequestParseType;
import com.set.app.voicenews.util.DebugLog;
import com.set.app.voicenews.util.GetNewReqTokenUtil;
import com.set.app.voicenews.util.connect.ConnectionConstant;
import com.set.app.voicenews.util.prefs.SetReqTokenUtil;
import com.set.app.voicenews.view.recyclerview.ViewTypeConstant;
import com.set.app.voicenews.view.recyclerview.ViewTypeInfo;
import com.set.app.voicenews.view.recyclerview.bindview.BVNewsList;
import com.set.app.voicenews.view.recyclerview.createview.CVGeneralList;
import com.set.app.voicenews.view.recyclerview.typeconvert.CTNewsList;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ResonTeng on 2017/1/11.
 */
public class FragSearchNewsResult extends Fragment {
    private static final String TAG             = "FragSearchNewsResult";
    public static final String FRAG_TRANS_NAME 	= "FragSearchNewsResult";
    private static Context mContext             = null;

    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mRecyclerViewAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayoutManager layoutManager;

    private RelativeLayout noResultRL;
    private ProgressBar mProgressBar;
    private boolean loadingFlag = false;
    private RelativeLayout mRefreshRL;
    private TextView mAlertTV;

    private int loadingPage = 1;
    private String searchKeyword;

    private DataInfo.SearchDO mSearchDO;
    private DataInfo.SearchNewsDO mSearchNewsDO;

    private static final int LIST_INIT = 0;
    private static final int LIST_LOAD_MORE = 1;
    private static final int LIST_REFRESH = 2;
    private static final int LIST_END = 3;
    private int nowListLoadStatus = LIST_INIT;

    public static FragSearchNewsResult newInstance(String keyword) {
        FragSearchNewsResult fragment = new FragSearchNewsResult();
        Bundle b = new Bundle();
        b.putString("data", keyword);
        fragment.setArguments(b);
        return fragment;
    }

    public FragSearchNewsResult() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        searchKeyword = getArguments().getString("data");
    }

    private View currentLayout = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View nowLayout = null;
        if (currentLayout == null) {
            nowLayout = inflater.inflate(R.layout.frag_search_news_result, null);

            setHasOptionsMenu(true);
            mToolbar = (Toolbar) nowLayout.findViewById(R.id.toolbar);
            ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mToolbar.setTitle(getString(R.string.search_news_result_title));
            if (Build.VERSION.SDK_INT >= 21) {
                mToolbar.setPadding(0, GlobalConstant.Status_Bar_Height, 0, 0);
            }

            noResultRL = (RelativeLayout) nowLayout.findViewById(R.id.noResultRL);
            mProgressBar = (ProgressBar) nowLayout.findViewById(R.id.loadingProgressBar);
            mRefreshRL = (RelativeLayout) nowLayout.findViewById(R.id.refreshRL);
            mRefreshRL.setOnClickListener(retryRequestListener);
            mAlertTV = (TextView) nowLayout.findViewById(R.id.alertTV);

            mRecyclerView = (RecyclerView) nowLayout.findViewById(R.id.recyclerView);
            mSwipeRefreshLayout = (SwipeRefreshLayout) nowLayout.findViewById(R.id.swipeRefreshLayout);
            mSwipeRefreshLayout.setColorSchemeResources(R.color.black, R.color.black, R.color.black); //設置載入顏色
            mSwipeRefreshLayout.setEnabled(true); //啟用下拉更新
            mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
                @Override
                public void onRefresh() {
                    if(loadingFlag){
                        return;// do nothing
                    }
                    DebugLog.d(TAG, "onRefresh");
                    loadingPage = 1;
                    nowListLoadStatus = LIST_REFRESH;
                    dataRequest();
                }
            });

            layoutManager = new LinearLayoutManager(mContext);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            mRecyclerView.setLayoutManager(layoutManager);

            currentLayout = nowLayout;

            dataRequest();

        } else {
            DebugLog.d(TAG, "nowLayout != null");
            ViewGroup parent = (ViewGroup) currentLayout.getParent();
            if (parent != null) {
                parent.removeView(currentLayout);
            }
            nowLayout = currentLayout;
        }

        return nowLayout;
    }

    private void dataRequest(){
        if(loadingFlag){//資料截取中
            return;
        }
        RequestPara mRequrestPara = new RequestPara();
        mRequrestPara.httpMethod 	= RequestPara.HTTP_METHOD_GET;
        mRequrestPara.parseType   	= RequestParseType.ParseType.SearchNewsResult;
        mRequrestPara.requestURL 	= getRequestURL();

        ApiRequestControl mApiRequestControl = new ApiRequestControl(requestCallBack, mRequrestPara, true);
        mApiRequestControl.startRequest();
        if(nowListLoadStatus == LIST_INIT) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
        noResultRL.setVisibility(View.GONE);
        loadingFlag = true;
    }

    private String getRequestURL(){
        String url = "";
        String keyword = searchKeyword;
        try {
            keyword = URLEncoder.encode(keyword, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            DebugLog.d(TAG, e.toString());
        }
        String category = "news";
        String page_wanted = String.valueOf(loadingPage);
        SetReqTokenUtil mSetReqTokenUtil = SetReqTokenUtil.getInstance(mContext);
        String token = mSetReqTokenUtil.getReqTokenInfo().token;

        url = String.format(APIConstant.API_Search_Articles, keyword, category, page_wanted, token);
        DebugLog.d(TAG, "url:" + url);
        return url;
    }

    private ApiRequestControl.CallBack requestCallBack = new ApiRequestControl.CallBack() {
        @Override
        public void doCallBack(Object obj) {
            RequestPara requrestPara = (RequestPara) obj;
            RequestParseType.ParseType parseType = requrestPara.parseType;

            switch (parseType) {
                case SearchNewsResult:
                    DebugLog.d(TAG, "SearchNewsResult!");

                    int httpStatus = requrestPara.httpStatus;
                    DebugLog.d(TAG, "httpStatus:" + httpStatus);
                    if(ConnectionConstant.checkConnectError(httpStatus)){
                        String alertStr = "連線異常 請確認網路狀態請稍後再試！";
                        showAlert(alertStr);
                        finishResponse();
                        return;
                    }
                    if(httpStatus!= HttpURLConnection.HTTP_OK){
                        String alertStr = "Server連線異常請稍後再試！";
                        showAlert(alertStr);
                        finishResponse();
                        return;
                    }

                    if(requrestPara.responseObject==null && requrestPara.responseStrContent.indexOf("token expired")>-1){
                        DebugLog.d(TAG, "responseStrContent:"+requrestPara.responseStrContent);
                        getTokenAgain();
                        return;
                    }
                    if(requrestPara.responseObject==null && requrestPara.responseStrContent.indexOf("invalid token")>-1){
                        String alertStr = "invalid token"; showAlert(alertStr); finishResponse();
                        return;
                    }

                    mSearchDO = (DataInfo.SearchDO) requrestPara.responseObject;
                    if(mSearchDO==null){
                        String alertStr = "資料解析異常請稍後再試！";
                        showAlert(alertStr);
                        finishResponse();
                        return;
                    }

                    mSearchNewsDO = mSearchDO.data;
                    if(mSearchDO.success && mSearchNewsDO.articles.size()>0){
                        CTNewsList mCTNewsList = new CTNewsList(mContext);
                        List<ViewTypeInfo> mViewTypeList ;
                        mViewTypeList = mCTNewsList.convertNewsList(filterRepeatNewsDOList());
                        resetAdapter(mViewTypeList);

                        if(mSearchNewsDO.current_page == mSearchNewsDO.total_pages){
                            nowListLoadStatus = LIST_END;
//                            Toast.makeText(mContext, "已無資料！", Toast.LENGTH_SHORT).show();
                        }else{
                            loadingPage = mSearchNewsDO.current_page + 1;
                            nowListLoadStatus = LIST_LOAD_MORE;
                        }
                    }else{
                        nowListLoadStatus = LIST_END;
                        mRecyclerView.setAdapter(null);
//                        Toast.makeText(mContext, "查無資料！", Toast.LENGTH_SHORT).show();
                        Snackbar.make(mRecyclerView, "查無資料！", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        noResultRL.setVisibility(View.VISIBLE);
                    }

                    finishResponse();
            }
        }
    };

    private void getTokenAgain(){
        DebugLog.i(TAG,"token expired getTokenAgain");
        GetNewReqTokenUtil mGetNewReqTokenUtil = new GetNewReqTokenUtil(mContext);
        mGetNewReqTokenUtil.setGetTokenListener(new GetNewReqTokenUtil.GetTokenListener(){
            @Override
            public void haveToken(DataInfo.TokenInfo token) {
                loadingFlag = false;
//                testOldToken="";
                dataRequest();
            }
            @Override
            public void connectErr(String error) {
                showAlert(error);
                finishResponse();
            }
        });
        mGetNewReqTokenUtil.tokenRequest();
    }

    //篩選重複新聞
    private Set<String> filterSet;
    private List<DataInfo.NewsDO> filterRepeatNewsDOList(){
        List<DataInfo.NewsDO> list = new ArrayList<DataInfo.NewsDO>();
        if(nowListLoadStatus == LIST_INIT || nowListLoadStatus == LIST_REFRESH){
            filterSet = new HashSet<>();
        }
        for (int i = 0; i < mSearchNewsDO.articles.size(); i++) {
            DataInfo.NewsDO mNewsDO = mSearchNewsDO.articles.get(i);
            if (filterSet.contains(mNewsDO.url)) {
                DebugLog.d(TAG, "RepeatNewsDO:" + mNewsDO.title);
            } else {
                list.add(mNewsDO);
                filterSet.add(mNewsDO.url);
            }
        }
        return list;
    }

    /**
     * list無法正常取得 showAlert
     * **/
    public void showAlert(String alert){
        if(nowListLoadStatus == LIST_INIT){
            mAlertTV.setText(alert);
            mRefreshRL.setVisibility(View.VISIBLE);
            return;
        }
        if(nowListLoadStatus == LIST_REFRESH){// Refresh異常
//            Toast.makeText(mContext, alert, Toast.LENGTH_SHORT).show();
            try {
                Snackbar.make(mRecyclerView, alert, Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }catch (Exception e){}
            mProgressBar.setVisibility(View.GONE);
            return;
        }
        if(nowListLoadStatus == LIST_LOAD_MORE){
            try {
                Snackbar.make(mRecyclerView, alert, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                //Remove loading item
                int removeId = mRecyclerViewAdapter.viewTypeList.size()-1;
                mRecyclerViewAdapter.viewTypeList.remove(removeId);
                mRecyclerViewAdapter.notifyItemRemoved(removeId);
            }catch (Exception e){}
            mProgressBar.setVisibility(View.GONE);
            return;
        }
    }

    private void finishResponse(){
        mProgressBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
        loadingFlag = false;
        mProgressBar.setVisibility(View.GONE);
        DebugLog.d(TAG, "-----------");
    }

    /**
     * list無法正常取得 retry
     * **/
//    public void retryRequest(String alert){
//        if(mRecyclerViewAdapter!=null){
//            Toast.makeText(mContext, alert, Toast.LENGTH_SHORT).show();
//            return;
//        }
//        mAlertTV.setText(alert);
//        mRefreshRL.setVisibility(View.VISIBLE);
//    }
    private View.OnClickListener retryRequestListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            mRefreshRL.setVisibility(View.GONE);
            dataRequest();
        }
    };

    private void resetAdapter(List<ViewTypeInfo> viewTypeList) {
        if (nowListLoadStatus == LIST_INIT || nowListLoadStatus == LIST_REFRESH) {
            mRecyclerViewAdapter = new RecyclerViewAdapter(viewTypeList);
            mRecyclerView.setAdapter(mRecyclerViewAdapter);

            mRecyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() { //Load more data for reyclerview
                    DebugLog.d(TAG, "Load More");

                    if (mRecyclerViewAdapter.viewTypeList.size()<1 ||
                            mRecyclerViewAdapter.viewTypeList.get(mRecyclerViewAdapter.viewTypeList.size()-1)
                                    .viewType == ViewTypeConstant.GeneralCellType.ITEM_LOADING){
                        return;
                    }

                    ViewTypeInfo mViewTypeInfo = new ViewTypeInfo();
                    mViewTypeInfo.viewType = ViewTypeConstant.GeneralCellType.ITEM_LOADING;
                    mRecyclerViewAdapter.viewTypeList.add(mViewTypeInfo);
                    mRecyclerViewAdapter.notifyItemInserted(mRecyclerViewAdapter.viewTypeList.size()-1);

                    dataRequest();
                }
            });
        }else{
            //Remove loading item
            int removeId = mRecyclerViewAdapter.viewTypeList.size()-1;
            mRecyclerViewAdapter.viewTypeList.remove(removeId);
            mRecyclerViewAdapter.notifyItemRemoved(removeId);

            mRecyclerViewAdapter.addMoreItem(viewTypeList);
            mRecyclerViewAdapter.notifyDataSetChanged();
            mRecyclerViewAdapter.setLoaded();
        }
    }

    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<ViewTypeInfo> viewTypeList;
        private CVGeneralList mCViewSearchNews;
        private BVNewsList mBVNewsList;
        private OnLoadMoreListener mOnLoadMoreListener;
        private int newsListSize = 0;

        private boolean loading = true;
        private int pastVisiblesItems, visibleItemCount, totalItemCount, lastVisibleItem;

        public RecyclerViewAdapter(List<ViewTypeInfo> viewTypeList) {
            this.viewTypeList = viewTypeList;
            mCViewSearchNews  = CVGeneralList.getInstance();
            mBVNewsList  = new BVNewsList(mContext, viewTypeList, true);
            setNewsList();

            mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();
                    lastVisibleItem =  layoutManager.findLastVisibleItemPosition();

//                    DebugLog.d(TAG,"visibleItemCount:"+visibleItemCount);
//                    DebugLog.d(TAG,"totalItemCount:"+totalItemCount);
//                    DebugLog.d(TAG,"pastVisiblesItems:"+pastVisiblesItems);
//                    DebugLog.d(TAG,"lastVisibleItem:"+lastVisibleItem);
//                    DebugLog.d(TAG, "-----------");

                    if (loading && nowListLoadStatus != LIST_END) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            DebugLog.d(TAG, "Last Item Now !");
                            loading = false;

                            nowListLoadStatus = LIST_LOAD_MORE;
                            DebugLog.d(TAG, "nowListLoadStatus:"+nowListLoadStatus);

                            if (mOnLoadMoreListener != null) {
                                mOnLoadMoreListener.onLoadMore();
                            }
                        }
                    }

                    if(loading && nowListLoadStatus==LIST_END){
                        if (totalItemCount>visibleItemCount && (visibleItemCount+pastVisiblesItems) >= totalItemCount){
                            loading = false;
//                            Toast.makeText(mContext, "已無資料！", Toast.LENGTH_SHORT).show();
                            Snackbar.make(mRecyclerView, "已無資料！", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                        }
                    }
                }
            });
        }

        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.mOnLoadMoreListener = mOnLoadMoreListener;
        }

        private void addMoreItem(List<ViewTypeInfo> viewTypeList) {
            this.viewTypeList.addAll(viewTypeList);
            this.notifyDataSetChanged();
            setNewsList();
        }

        @Override
        public int getItemCount() {
            return viewTypeList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return viewTypeList.get(position).viewType;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = mCViewSearchNews.getViewHolder(parent, viewType);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            mBVNewsList.setViewHolder(viewHolder, position);
        }

        @Override
        public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
            super.onViewDetachedFromWindow(holder);
            holder.itemView.clearAnimation();
        }

        public void setLoaded() {
            loading = true;
        }

        private void setNewsList(){
            List<Object> mNewsList = new ArrayList<>();
            for(int i =0; i<viewTypeList.size() ;i++){
                ViewTypeInfo mViewTypeInfo = viewTypeList.get(i);
                int type = mViewTypeInfo.viewType;
                if(type == ViewTypeConstant.GeneralCellType.ITEM_PTT_NEWS
                        ||type == ViewTypeConstant.GeneralCellType.ITEM_FANS_NEWS
                        ||type == ViewTypeConstant.GeneralCellType.ITEM_NEWS
                        ){
                    mNewsList.add(mViewTypeInfo.dataObject);
                }
            }
            mBVNewsList.setNewsList(mNewsList);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                DebugLog.d(TAG, "home");
                ((MainActivity)mContext).onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item); // important line
    }

    private interface OnLoadMoreListener {
        void onLoadMore();
    }
}
