package com.set.app.voicenews.view.recyclerview.createview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.set.app.voicenews.R;
import com.set.app.voicenews.view.recyclerview.ViewTypeConstant;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class CVGeneralList {

    public static CVGeneralList getInstance() {
        return new CVGeneralList();
    }

    public CVGeneralList (){}

    public RecyclerView.ViewHolder getViewHolder(ViewGroup parent, int viewType){
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ViewTypeConstant.GeneralCellType.ITEM_PTT_NEWS:
                View vHolderPttNews = mInflater.inflate(R.layout.list_item_ptt_news, parent, false);
                viewHolder = new HolderPttNews(vHolderPttNews);
                break;
            case ViewTypeConstant.GeneralCellType.ITEM_FANS_NEWS:
                View vHolderFansNews = mInflater.inflate(R.layout.list_item_fans_news, parent, false);
                viewHolder = new HolderFansNews(vHolderFansNews);
                break;
            case ViewTypeConstant.GeneralCellType.ITEM_NEWS:
                View vHolderNews = mInflater.inflate(R.layout.list_item_news, parent, false);
                viewHolder = new HolderNews(vHolderNews);
                break;
            case ViewTypeConstant.GeneralCellType.ITEM_GRID_PAIR_NEWS_SOURCE:
                View vHolderGridPair = mInflater.inflate(R.layout.list_item_grid_pair, parent, false);
                viewHolder = new HolderGridPair(vHolderGridPair);
                break;

            case ViewTypeConstant.GeneralCellType.ITEM_LOADING:
                View vLoading = mInflater.inflate(R.layout.list_item_loading, parent, false);
                viewHolder = new HolderItemLoading(vLoading);
                break;

            case ViewTypeConstant.GeneralCellType.ITEM_DESCRIPTION_TITIE:
                View vDescribeTitle = mInflater.inflate(R.layout.list_item_describe_title, parent, false);
                viewHolder = new HolderDescriptionTitle(vDescribeTitle);
                break;
            case ViewTypeConstant.GeneralCellType.ITEM_SEARCH_RECORD_CONTENT:
                View vSearchRecord = mInflater.inflate(R.layout.list_item_search_record, parent, false);
                viewHolder = new HolderSearchRecordContent(vSearchRecord);
                break;

        }
        return viewHolder;
    }

    public class HolderPttNews extends RecyclerView.ViewHolder{
        public RelativeLayout contentRL;
        public TextView sourceTV ;
        public TextView dateTV ;
        public TextView titleTV ;
        public TextView countTV ;

        public HolderPttNews(View itemView) {
            super(itemView);
            contentRL   = (RelativeLayout) itemView.findViewById(R.id.contentRL);
            sourceTV    = (TextView) itemView.findViewById(R.id.sourceTV);
            dateTV      = (TextView) itemView.findViewById(R.id.dateTV);
            titleTV     = (TextView) itemView.findViewById(R.id.titleTV);
            countTV     = (TextView) itemView.findViewById(R.id.countTV);
        }
    }

    public class HolderFansNews extends RecyclerView.ViewHolder{
        public RelativeLayout contentRL;
        public TextView sourceTV ;
        public TextView dateTV ;
        public TextView titleTV ;
        public ImageView countIV;
        public TextView countTV ;

        public HolderFansNews(View itemView) {
            super(itemView);
            contentRL   = (RelativeLayout) itemView.findViewById(R.id.contentRL);
            sourceTV    = (TextView) itemView.findViewById(R.id.sourceTV);
            dateTV      = (TextView) itemView.findViewById(R.id.dateTV);
            titleTV     = (TextView) itemView.findViewById(R.id.titleTV);
            countIV     = (ImageView) itemView.findViewById(R.id.countIV);
            countTV     = (TextView) itemView.findViewById(R.id.countTV);
        }
    }

    public class HolderNews extends RecyclerView.ViewHolder{
        public RelativeLayout contentRL;
        public ImageView sourceIV;
        public TextView sourceTV ;
        public TextView dateTV ;
        public TextView titleTV ;
        public TextView countTV ;

        public HolderNews(View itemView) {
            super(itemView);
            contentRL  = (RelativeLayout) itemView.findViewById(R.id.contentRL);
            sourceIV    = (ImageView) itemView.findViewById(R.id.sourceIV);
            sourceTV    = (TextView) itemView.findViewById(R.id.sourceTV);
            dateTV      = (TextView) itemView.findViewById(R.id.dateTV);
            titleTV     = (TextView) itemView.findViewById(R.id.titleTV);
            countTV     = (TextView) itemView.findViewById(R.id.countTV);
        }
    }

    public class HolderGridPair extends RecyclerView.ViewHolder{
        public View gridLeftRL;
        public ImageView leftIV ;
        public ImageView editLeftIV ;

        public View gridRightRL;
        public ImageView rightIV ;
        public ImageView editRightIV ;

        public HolderGridPair(View itemView) {
            super(itemView);
            gridLeftRL  = itemView.findViewById(R.id.gridLeftRL);
            leftIV  = (ImageView) itemView.findViewById(R.id.leftIV);
            editLeftIV  = (ImageView) itemView.findViewById(R.id.editLeftIV);

            gridRightRL = itemView.findViewById(R.id.gridRightRL);
            rightIV     = (ImageView) itemView.findViewById(R.id.rightIV);
            editRightIV = (ImageView) itemView.findViewById(R.id.editRightIV);
        }
    }

    public class HolderItemLoading extends RecyclerView.ViewHolder {
        public ProgressBar HintPB;
        public TextView HintTV;

        public HolderItemLoading(View itemView) {
            super(itemView);
            HintPB = (ProgressBar) itemView.findViewById(R.id.recyclerview_loadmore_hint_pb);
            HintTV = (TextView) itemView.findViewById(R.id.recyclerview_loadmore_hint_tv);
        }
    }

    public class HolderDescriptionTitle extends RecyclerView.ViewHolder {
        public TextView describeTitle;
        public View divider;

        public HolderDescriptionTitle(View itemView) {
            super(itemView);
            describeTitle  = (TextView) itemView.findViewById(R.id.describeTitle);
            divider = (View) itemView.findViewById(R.id.divide);
        }
    }

    public class HolderSearchRecordContent extends RecyclerView.ViewHolder {
        public TextView recordNameTV;
        public ImageView recordCleanIV;
        public RelativeLayout recordItemRL;

        public HolderSearchRecordContent(View itemView) {
            super(itemView);
            recordNameTV  = (TextView) itemView.findViewById(R.id.recordNameTV);
            recordCleanIV = (ImageView) itemView.findViewById(R.id.recordCleanIV);
            recordItemRL = (RelativeLayout) itemView.findViewById(R.id.recordItemRL);
        }
    }
}
