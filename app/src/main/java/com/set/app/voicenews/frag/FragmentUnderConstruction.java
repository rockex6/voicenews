package com.set.app.voicenews.frag;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.set.app.voicenews.R;
import com.set.app.voicenews.util.DebugLog;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class FragmentUnderConstruction extends Fragment {

    private static final String TAG = "FragmentUnderConstruction";

    private Context mContext;

    private TextView mControlTV;
    private String name;

    public static FragmentUnderConstruction newInstance(String name) {

        FragmentUnderConstruction frag = new FragmentUnderConstruction();
        Bundle b = new Bundle();
        b.putString("name", name);
        frag.setArguments(b);
        return frag;

    }

    public FragmentUnderConstruction(){}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext        = context;
        name = getArguments().getString("name");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        DebugLog.d(TAG, "FragmentUnderConstruction onCreateView");

        View nowLayout  = inflater.inflate(R.layout.under_construction, null);
        mControlTV = (TextView) nowLayout.findViewById(R.id.controlTV);
        mControlTV.setText(name);

        return nowLayout;
    }
}
