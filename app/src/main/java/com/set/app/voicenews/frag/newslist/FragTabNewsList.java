package com.set.app.voicenews.frag.newslist;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.set.app.voicenews.R;
import com.set.app.voicenews.constant.APIConstant;
import com.set.app.voicenews.constant.GlobalConstant;
import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.frag.base.BaseRecyclerFragment;
import com.set.app.voicenews.requestcontrol.ApiRequestControl;
import com.set.app.voicenews.requestcontrol.RequestPara;
import com.set.app.voicenews.requestcontrol.RequestParseType;
import com.set.app.voicenews.util.DebugLog;
import com.set.app.voicenews.util.GetNewReqTokenUtil;
import com.set.app.voicenews.util.connect.ConnectionConstant;
import com.set.app.voicenews.util.prefs.SetNewsSourceUtil;
import com.set.app.voicenews.util.prefs.SetReqTokenUtil;
import com.set.app.voicenews.view.recyclerview.ViewTypeConstant;
import com.set.app.voicenews.view.recyclerview.ViewTypeInfo;
import com.set.app.voicenews.view.recyclerview.bindview.BVNewsList;
import com.set.app.voicenews.view.recyclerview.createview.CVGeneralList;
import com.set.app.voicenews.view.recyclerview.typeconvert.CTNewsList;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by kennethyeh on 2017/1/3.
 */

public class FragTabNewsList extends BaseRecyclerFragment {

    private static final String TAG = "FragTabNewsList";

    private Context mContext;
    private static final String keyNewsTabInfo = "keyNewsTabInfo";
    private DataInfo.NewsTabInfo mNewsTabInfo;

    private boolean loadingFlag = false;
    private ApiRequestControl mApiRequestControl;

    private int nowListLoadStatus   = LIST_INIT;

    private RecyclerViewAdapter mRecyclerViewAdapter;

    public static FragTabNewsList newInstance(DataInfo.NewsTabInfo newsTabInfo) {

        FragTabNewsList frag = new FragTabNewsList();
        Bundle b = new Bundle();
        b.putSerializable(keyNewsTabInfo, newsTabInfo);
        frag.setArguments(b);
        return frag;
    }

    public FragTabNewsList(){}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext        = context;
        mNewsTabInfo    = (DataInfo.NewsTabInfo) getArguments().getSerializable(keyNewsTabInfo);
    }


    @Override
    protected void init() {
        dataRequest();
    }
    @Override
    protected void resumeFrag() {

    }

    private synchronized void dataRequest(){
        if(loadingFlag){//資料截取中
            return;
        }
        RequestPara mRequrestPara       = new RequestPara();
        mRequrestPara.httpMethod 	    = RequestPara.HTTP_METHOD_GET;
        if(mNewsTabInfo.category == GlobalConstant.CateType.VOICE_PTT){
            mRequrestPara.parseType   	    = RequestParseType.ParseType.PttNewsList;
        }else if(mNewsTabInfo.category == GlobalConstant.CateType.VOICE_FANS_NEWS){
            mRequrestPara.parseType   	    = RequestParseType.ParseType.FansNewsList;
        }else if(mNewsTabInfo.category == GlobalConstant.CateType.VOICE_NEWS_MULTI){
            mRequrestPara.parseType   	    = RequestParseType.ParseType.NewsList;
        }
        mRequrestPara.requestURL 	    = getRequestURL();

        mApiRequestControl 			= new ApiRequestControl(requestCallBack, mRequrestPara, true);
        mApiRequestControl.startRequest();
        mRefreshRL.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.GONE);
        if(nowListLoadStatus==LIST_INIT){
            mProgressBar.setVisibility(View.VISIBLE);
        }
        loadingFlag = true;
    }


//    String testOldToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InNhbmxpaC5wbXNAZ21haWwuY29tIiwiaXNzdWVkIjoiMjAxNjEyMzAwOTExNTQiLCJleHBpcmF0aW9uIjoiMjAxNjEyMzEwOTExNTQifQ.cqfPhHggTp2geflwvrzK-05sf0OYu3bLbILhecZiFfo";
    private String getRequestURL() {
        String url = "";
        String category = "";
        String orderBase = "";
        String orderType = "";
        SetReqTokenUtil mSetReqTokenUtil = SetReqTokenUtil.getInstance(mContext);
//        String token = (!testOldToken.equalsIgnoreCase(""))?(testOldToken):(mSetReqTokenUtil.getReqTokenInfo().token);
        String token = mSetReqTokenUtil.getReqTokenInfo().token;
        DebugLog.i(TAG, "category:" + mNewsTabInfo.category);
        DebugLog.i(TAG, "orderType:" + mNewsTabInfo.orderType);

        int checkCategory = mNewsTabInfo.category;

        category = mNewsTabInfo.categoryStr;

        if (checkCategory == GlobalConstant.CateType.VOICE_PTT
         || checkCategory == GlobalConstant.CateType.VOICE_NEWS_MULTI) {
            orderType = mNewsTabInfo.orderType;
            url = String.format(APIConstant.API_Get_Articles_Order_Type, category, orderType, token);
        }
        if (checkCategory == GlobalConstant.CateType.VOICE_FANS_NEWS) {
            orderBase = mNewsTabInfo.orderBase;
            url = String.format(APIConstant.API_Get_Articles_Order_Base, category, orderBase, token);
        }

        DebugLog.d(TAG, "url:" + url);
        return url;
    }

    private ApiRequestControl.CallBack requestCallBack = new ApiRequestControl.CallBack() {

        @Override
        public void doCallBack(Object obj) {
            RequestPara requrestPara = (RequestPara) obj;
            RequestParseType.ParseType parseType = requrestPara.parseType;

            DebugLog.d(TAG, "SearchNewsrResultList!");
            DebugLog.d(TAG, "nowListLoadStatus:"+nowListLoadStatus);
            int httpStatus = requrestPara.httpStatus;
            DebugLog.d(TAG, "httpStatus:"+httpStatus);
//                    DebugLog.d(TAG, "responseStrContent:"+requrestPara.responseStrContent);
            if(ConnectionConstant.checkConnectError(httpStatus)){
                String alertStr = "連線異常 請確認網路狀態請稍後再試！";
                showAlert(alertStr);
                finishResponse();
                return;
            }
            if(httpStatus!= HttpURLConnection.HTTP_OK){
                String alertStr = "Server連線異常請稍後再試！";
                showAlert(alertStr);
                finishResponse();
                return;
            }
            if(requrestPara.responseObject==null && requrestPara.responseStrContent.indexOf("token expired")>-1){
                DebugLog.d(TAG, "responseStrContent:"+requrestPara.responseStrContent);
                getTokenAgain();
                return;
            }
            if(requrestPara.responseObject==null && requrestPara.responseStrContent.indexOf("invalid token")>-1){
                String alertStr = "invalid token"; showAlert(alertStr); finishResponse();
                return;
            }
            // PTT
            if(parseType == RequestParseType.ParseType.PttNewsList) {
                DataInfo.PttNewsList mPttNewsList = (DataInfo.PttNewsList) requrestPara.responseObject;
                if(mPttNewsList==null){ String alertStr = "資料異常"; showAlert(alertStr); finishResponse();
                    return;
                }
                CTNewsList mCTNewsList = new CTNewsList(mContext);
                List<ViewTypeInfo> mViewTypeList = mCTNewsList.convertPttNewsList(mPttNewsList.data);
                setAdapter(mViewTypeList);
            }
            // FB FANS
            if(parseType == RequestParseType.ParseType.FansNewsList){
                DataInfo.NewsList mNewsList = (DataInfo.NewsList) requrestPara.responseObject;
                if(mNewsList==null){String alertStr = "資料異常"; showAlert(alertStr); finishResponse();
                    return;
                }
                CTNewsList mCTNewsList = new CTNewsList(mContext);
                List<ViewTypeInfo> mViewTypeList = mCTNewsList.convertFansNewsList(mNewsList.data);
                setAdapter(mViewTypeList);
            }
            // 話題
            if(parseType == RequestParseType.ParseType.NewsList){
                DataInfo.NewsList mNewsList = (DataInfo.NewsList) requrestPara.responseObject;
                if(mNewsList==null){String alertStr = "資料異常"; showAlert(alertStr); finishResponse();
                    return;
                }
                CTNewsList mCTNewsList = new CTNewsList(mContext);

                List<DataInfo.NewsDO> filterDataList = new ArrayList<>(); // 過濾list
                SetNewsSourceUtil mSetNewsSourceUtil = SetNewsSourceUtil.getInstance(mContext);
                for(int i =0 ; i<mNewsList.data.size() ; i++){
                    DataInfo.NewsDO mNewsDO = mNewsList.data.get(i);
                    String source = mNewsDO.source_id;
                    if(mSetNewsSourceUtil.getNewsSourcePref(source)){ // 喜愛的分類新聞才顯示
                        filterDataList.add(mNewsDO);
                    }
                }
                if(filterDataList.size()<1){
                    String alertStr = "無分類新聞！";
                    showAlert(alertStr);
                }
//                List<ViewTypeInfo> mViewTypeList = mCTNewsList.convertNewsList(mNewsList.data);
                List<ViewTypeInfo> mViewTypeList = mCTNewsList.convertNewsList(filterDataList);
                setAdapter(mViewTypeList);
            }
            finishResponse();
        }
    };

    private void getTokenAgain(){
        DebugLog.i(TAG,"token expired getTokenAgain");
        GetNewReqTokenUtil mGetNewReqTokenUtil = new GetNewReqTokenUtil(mContext);
        mGetNewReqTokenUtil.setGetTokenListener(new GetNewReqTokenUtil.GetTokenListener(){
            @Override
            public void haveToken(DataInfo.TokenInfo token) {
                loadingFlag = false;
//                testOldToken="";
                dataRequest();
            }
            @Override
            public void connectErr(String error) {
                showAlert(error);
                finishResponse();
            }
        });
        mGetNewReqTokenUtil.tokenRequest();
    }


    /**
     * list無法正常取得 showAlert
     * **/
    public void showAlert(String alert){
        mAlertTV.setText(alert);
        mRefreshRL.setVisibility(View.VISIBLE);
        if(nowListLoadStatus == LIST_REFRESH){// Refresh異常
//            Toast.makeText(mContext, alert, Toast.LENGTH_SHORT).show();
            DebugLog.d(TAG,"getUserVisibleHint:"+(FragTabNewsList.this.getUserVisibleHint()));
            try {
                if(FragTabNewsList.this.getUserVisibleHint()){
                    Snackbar.make(mRecyclerView, alert, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            }catch (Exception e){}
            mProgressBar.setVisibility(View.GONE);
            return;
        }

    }

    private void finishResponse(){
        mProgressBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
        loadingFlag = false;
        mProgressBar.setVisibility(View.GONE);
        DebugLog.d(TAG, "-----------");
    }


    private void setAdapter(List<ViewTypeInfo> viewTypeList){
        if(mRecyclerViewAdapter!=null){
            mRecyclerViewAdapter.removeAll();
        }
        mRecyclerViewAdapter = new RecyclerViewAdapter(viewTypeList);
        mRecyclerView.setAdapter(mRecyclerViewAdapter);
    }

    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<ViewTypeInfo> viewTypeList;
        private CVGeneralList mCVGeneralList;
        private BVNewsList mBVNewsList;

        public RecyclerViewAdapter(List<ViewTypeInfo> viewTypeList){
            this.viewTypeList   = viewTypeList;
            mCVGeneralList      = new CVGeneralList();
            mBVNewsList     = new BVNewsList(mContext, viewTypeList);
            mBVNewsList.setNewsTabInfo(mNewsTabInfo);
            setNewsList();
        }

        @Override
        public int getItemCount() {
            return viewTypeList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return viewTypeList.get(position).viewType;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = mCVGeneralList.getViewHolder(parent, viewType);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            mBVNewsList.setViewHolder(viewHolder, position);
        }

        @Override
        public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
            super.onViewDetachedFromWindow(holder);
            holder.itemView.clearAnimation();
        }

        public void removeAll(){
            viewTypeList.removeAll(viewTypeList);
            setNewsList();
            mRecyclerViewAdapter.notifyDataSetChanged();
        }

        private void setNewsList(){
            List<Object> mNewsList = new ArrayList<>();
            for(int i =0; i<viewTypeList.size() ;i++){
                ViewTypeInfo mViewTypeInfo = viewTypeList.get(i);
                int type = mViewTypeInfo.viewType;
                if(type == ViewTypeConstant.GeneralCellType.ITEM_PTT_NEWS
                ||type == ViewTypeConstant.GeneralCellType.ITEM_FANS_NEWS
                ||type == ViewTypeConstant.GeneralCellType.ITEM_NEWS
                ){
                    mNewsList.add(mViewTypeInfo.dataObject);
                }
            }
            mBVNewsList.setNewsList(mNewsList);
        }

    }

    @Override
    protected void setRetryRequestListener(RelativeLayout mRefreshRL) {
        mRefreshRL.setOnClickListener(retryRequestListener);
    }
    private View.OnClickListener retryRequestListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            mRefreshRL.setVisibility(View.GONE);
            if(nowListLoadStatus==LIST_INIT || nowListLoadStatus==LIST_REFRESH){
                init();
            }
        }
    };


    @Override
    protected void setOnRefreshListener(SwipeRefreshLayout mSwipeRefreshLayout) {
//        mSwipeRefreshLayout.setRefreshing(false);
//        mSwipeRefreshLayout.setEnabled(false);
        mSwipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.black, R.color.black, R.color.black);
    }
    private SwipeRefreshLayout.OnRefreshListener mOnRefreshListener = new SwipeRefreshLayout.OnRefreshListener(){
        @Override
        public void onRefresh() {
            if(loadingFlag){
                return;// do nothing
            }
            DebugLog.d(TAG, "onRefresh");
            nowListLoadStatus = LIST_REFRESH;
            dataRequest();
        }
    };


    @Override
    protected void setOnScrollListener(RecyclerView mRecyclerView) {

    }
    /**
     * FragCateNews 呼叫
     * **/
    public void editChangeCheck(){
        DebugLog.d(TAG, "editChangeCheck");
        // 有異動紀錄 重過濾列表
        nowListLoadStatus = LIST_REFRESH;
        dataRequest();
        mSwipeRefreshLayout.setRefreshing(true);
    }
}
