package com.set.app.voicenews.view.recyclerview.typeconvert;

import android.content.Context;

import com.set.app.voicenews.constant.APIConstant;
import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.view.recyclerview.ViewTypeConstant;
import com.set.app.voicenews.view.recyclerview.ViewTypeInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class CTNewsList {
    private final String TAG = "CTCateHomeNews";
    private Context mContext;
    public CTNewsList(Context mContext){

        this.mContext            = mContext;
    }

    public List<ViewTypeInfo> convertPttNewsList(List<DataInfo.PttNewsDO> list){
        List<ViewTypeInfo> mViewTypeList = new ArrayList<ViewTypeInfo>();
        for(int i =0;i<list.size();i++){
            ViewTypeInfo mViewTypeInfo = new ViewTypeInfo();
            DataInfo.PttNewsDO mPttNewsDO = list.get(i);
            mViewTypeInfo.viewType      = ViewTypeConstant.GeneralCellType.ITEM_PTT_NEWS;
            mViewTypeInfo.dataObject    = mPttNewsDO;
            mViewTypeList.add(mViewTypeInfo);
        }
        return mViewTypeList;
    }

    public List<ViewTypeInfo> convertFansNewsList(List<DataInfo.NewsDO> list){
        List<ViewTypeInfo> mViewTypeList = new ArrayList<ViewTypeInfo>();
        for(int i =0;i<list.size();i++){
            ViewTypeInfo mViewTypeInfo = new ViewTypeInfo();
            DataInfo.NewsDO mNewsDO = list.get(i);
            mViewTypeInfo.viewType      = ViewTypeConstant.GeneralCellType.ITEM_FANS_NEWS;
            mViewTypeInfo.dataObject    = mNewsDO;
            mViewTypeList.add(mViewTypeInfo);
        }

        return mViewTypeList;
    }

    public List<ViewTypeInfo> convertNewsList(List<DataInfo.NewsDO> list){
        List<ViewTypeInfo> mViewTypeList = new ArrayList<ViewTypeInfo>();
        for(int i =0;i<list.size();i++){
            ViewTypeInfo mViewTypeInfo = new ViewTypeInfo();
            DataInfo.NewsDO mNewsDO = list.get(i);

            if(mNewsDO.source_id.trim().equalsIgnoreCase("ettoday")){ //ETtoday News
                String ettodayNewsId = mNewsDO.url.substring(mNewsDO.url.length()-10, mNewsDO.url.length()-4);
                mNewsDO.url = String.format(APIConstant.ETtoday_News_API, ettodayNewsId);
//                DebugLog.d(TAG, "ettoday url: "+mNewsDO.url);
            }

            mViewTypeInfo.viewType      = ViewTypeConstant.GeneralCellType.ITEM_NEWS;
            mViewTypeInfo.dataObject    = mNewsDO;
            mViewTypeList.add(mViewTypeInfo);
        }

        return mViewTypeList;
    }
}
