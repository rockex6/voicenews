package com.set.app.voicenews.requestcontrol;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.parser.ParserNewsList;
import com.set.app.voicenews.parser.ParserNewsSource;
import com.set.app.voicenews.parser.ParserPttNewsList;
import com.set.app.voicenews.parser.ParserReqToken;
import com.set.app.voicenews.parser.ParserSearchNews;
import com.set.app.voicenews.parser.ParserSearchNewsResult;
import com.set.app.voicenews.util.DebugLog;
import com.set.app.voicenews.util.connect.WebServiceTask;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class ApiRequestControl {

    private static final String TAG = "ApiRequestControl";

    public static final int START_REQUEST 	= 1;
    public static final int PARSE_RESPONSE = 2;

    private CallBack mCallBack;
    private RequestPara mRequestPara;
    private boolean mResponseParsing = false;

    private AsyncTask mCurrentTask = null;

    public interface CallBack{
        void doCallBack(Object obj);
    }

    private Handler requestHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case START_REQUEST:
                    WebServiceTask mWebServiceTask =  new WebServiceTask(requestHandler, mRequestPara);
                    mWebServiceTask.execute();
                    mCurrentTask = mWebServiceTask;
                    break;
                case PARSE_RESPONSE:
                    if(!mResponseParsing){
                        mRequestPara = (RequestPara) msg.obj;
                        doCallback();
                    }else{
                        startParsing();
                    }
                    break;
            }
        }
    };

    /**
     * @param mCallBack
     *   呼叫頁面回傳結果callback
     * @param requrestPara
     *     存放http request參數＆response回傳狀態與內容
     * @param responseParsing
     *     是否對回傳內容解析
     *     true: 使用對應parser解析回傳內容字串
     *     fasle: 直接回傳內容字串 不做解析
     *
     * **/
    public ApiRequestControl(CallBack mCallBack, RequestPara requrestPara, boolean responseParsing){
        this.mCallBack 			= mCallBack;
        this.mRequestPara 		= requrestPara;
        this.mResponseParsing 	= responseParsing;
    }

    public void startRequest(){
        requestHandler.sendMessage(requestHandler.obtainMessage(START_REQUEST));
    }

    private void startParsing(){
        RequestParseType.ParseType parseType = mRequestPara.parseType;
        switch (parseType) {

            case GetRequestToken:
                try {
                    DataInfo.ReqTokenInfo mReqTokenInfo = new ParserReqToken().parsing(mRequestPara.responseStrContent);
                    mRequestPara.responseObject = mReqTokenInfo;
                } catch (Exception e) {
                    mRequestPara.responseObject=null;
                    DebugLog.d(TAG, "startParsing GetRequestToken Exception: "+e.toString());
                }
                break;

            case PttNewsList:
                try {
                    DataInfo.PttNewsList mPttNewsList = new ParserPttNewsList().parsing(mRequestPara.responseStrContent);
                    mRequestPara.responseObject = mPttNewsList;
                } catch (Exception e) {
                    mRequestPara.responseObject=null;
                    DebugLog.d(TAG, "startParsing PttNewsList Exception: "+e.toString());
                }
                break;

            case FansNewsList:
                try {
                    DataInfo.NewsList mNewsList = new ParserNewsList().parsing(mRequestPara.responseStrContent);
                    mRequestPara.responseObject = mNewsList;
                } catch (Exception e) {
                    mRequestPara.responseObject=null;
                    DebugLog.d(TAG, "startParsing FansNewsList Exception: "+e.toString());
                }
                break;

            case NewsList:
                try {
                    DataInfo.NewsList mNewsList = new ParserNewsList().parsing(mRequestPara.responseStrContent);
                    mRequestPara.responseObject = mNewsList;
                } catch (Exception e) {
                    mRequestPara.responseObject=null;
                    DebugLog.d(TAG, "startParsing NewsList Exception: "+e.toString());
                }
                break;

            case NewsSource:
                try {
                    DataInfo.NewsSource mNewsSource = new ParserNewsSource().parsing(mRequestPara.responseStrContent);
                    mRequestPara.responseObject = mNewsSource;
                } catch (Exception e) {
                    mRequestPara.responseObject=null;
                    DebugLog.d(TAG, "startParsing NewsSource Exception: "+e.toString());
                }
                break;

            case SearchNews:
                try {
                    DataInfo.SearchHotDO mSearchHotDO = new ParserSearchNews().parsing(mRequestPara.responseStrContent);
                    mRequestPara.responseObject = mSearchHotDO;
                } catch (Exception e) {
                    mRequestPara.responseObject=null;
                    DebugLog.d(TAG, "startParsing SearchNews Exception: "+e.toString());
                }
                break;

            case SearchNewsResult:
                try {
                    DataInfo.SearchDO mSearchDO = new ParserSearchNewsResult().parsing(mRequestPara.responseStrContent);
                    mRequestPara.responseObject = mSearchDO;
                } catch (Exception e) {
                    mRequestPara.responseObject=null;
                    DebugLog.d(TAG, "startParsing SearchNewsResult Exception: "+e.toString());
                }
                break;

        }
        doCallback();
    }

    private void doCallback(){
        mCallBack.doCallBack(mRequestPara);
    }

    /**
     * 當fragment destroy view時 取消背景task
     * **/
    public void cancelTask(){
        if(mCurrentTask!=null){
            try {
                if (mCurrentTask.getStatus().equals(AsyncTask.Status.RUNNING) ||
                        mCurrentTask.getStatus().equals(AsyncTask.Status.PENDING)) {
                    mCurrentTask.cancel(true);
//                    DebugLog.i(TAG, "cancel status:" + mCurrentTask.getStatus().toString());
                }
            }catch (Exception e){}

        }
    }
}
