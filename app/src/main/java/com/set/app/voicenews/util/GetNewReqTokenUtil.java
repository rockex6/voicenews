package com.set.app.voicenews.util;

import android.content.Context;

import com.set.app.voicenews.constant.APIConstant;
import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.requestcontrol.ApiRequestControl;
import com.set.app.voicenews.requestcontrol.RequestPara;
import com.set.app.voicenews.requestcontrol.RequestParseType;
import com.set.app.voicenews.util.prefs.SetReqTokenUtil;

import java.net.HttpURLConnection;

import static com.set.app.voicenews.util.connect.ConnectionConstant.checkConnectError;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class GetNewReqTokenUtil {

    private static final String TAG         = "GetNewReqTokenUtil";
    private Context mContext      = null;
    private ApiRequestControl mApiRequestControl;
    private GetTokenListener mGetTokenListener = null;

    public interface GetTokenListener{
        void haveToken(DataInfo.TokenInfo token);
        void connectErr(String error);
    }

    public GetNewReqTokenUtil(Context context){
        this.mContext           = context;
    }

    public void setGetTokenListener(GetTokenListener getTokenListener){
        this.mGetTokenListener  = getTokenListener;
    }

    public void tokenRequest(){

        RequestPara mRequrestPara = new RequestPara();
        mRequrestPara.httpMethod 	= RequestPara.HTTP_METHOD_GET;
        mRequrestPara.parseType   	= RequestParseType.ParseType.GetRequestToken;
        mRequrestPara.requestURL 	= getRequestURL();

        mApiRequestControl 			= new ApiRequestControl(requestCallBack, mRequrestPara, true);
        mApiRequestControl.startRequest();
    }

    private String getRequestURL(){
        String url = APIConstant.GET_REQ_TOKEN;
        DebugLog.i(TAG,"url:"+url);
        return url;
    }

    private ApiRequestControl.CallBack requestCallBack = new ApiRequestControl.CallBack() {

        @Override
        public void doCallBack(Object obj) {
            RequestPara requrestPara = (RequestPara) obj;
            RequestParseType.ParseType parseType = requrestPara.parseType;
            switch (parseType) {
                case GetRequestToken:

                    int httpStatus = requrestPara.httpStatus;
                    DebugLog.d(TAG, "httpStatus:" + httpStatus);
                    if(checkConnectError(httpStatus) || httpStatus!= HttpURLConnection.HTTP_OK){
                        String alertStr = "連線異常 請確認網路狀態請稍後再試！";
                        DebugLog.d(TAG, alertStr);
                        doErrorCallback(alertStr);
                        return;
                    }
                    DataInfo.ReqTokenInfo mReqTokenInfo = (DataInfo.ReqTokenInfo) requrestPara.responseObject;
                    if(!mReqTokenInfo.success){
                        String alertStr = "Request Token Fail";
                        DebugLog.d(TAG, alertStr);
                        doErrorCallback(alertStr);
                        return;
                    }
                    setToken(mReqTokenInfo.data);
                    break;
            }
        }
    };

    private void setToken(DataInfo.TokenInfo tokenInfo){
        DebugLog.d(TAG, "mtoken:"+tokenInfo.token);
        DebugLog.d(TAG, "expiration:"+tokenInfo.expiration);
        SetReqTokenUtil mSetReqTokenUtil = SetReqTokenUtil.getInstance(mContext);
        mSetReqTokenUtil.setTokenInfo(tokenInfo);
        doCallback(tokenInfo);
    }

    private void doCallback(DataInfo.TokenInfo token){
        if(mGetTokenListener!=null){
            mGetTokenListener.haveToken(token);
        }
    }

    private void doErrorCallback(String err){
        if(mGetTokenListener!=null){
            mGetTokenListener.connectErr(err);
        }
    }
}
