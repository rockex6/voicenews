package com.set.app.voicenews.frag.setting;


import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.set.app.voicenews.MainActivity;
import com.set.app.voicenews.R;
import com.set.app.voicenews.constant.GlobalConstant;
import com.set.app.voicenews.util.DebugLog;

/**
 * Created by ResonTeng on 2017/1/9.
 */
public class FragAboutMe extends Fragment {
    private static final String TAG            = "FragAboutMe";
    public static final String FRAG_TRANS_NAME = "FragAboutMe";
    private Context mContext;
    private Toolbar mToolbar;

    public static FragAboutMe newInstance() {
        FragAboutMe frag = new FragAboutMe();
        Bundle b = new Bundle();
        frag.setArguments(b);
        return frag;
    }

    public FragAboutMe(){}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DebugLog.d(TAG, TAG + " onCreateView");
        View nowLayout  = inflater.inflate(R.layout.frag_about_me, null);

        setHasOptionsMenu(true);
        mToolbar        = (Toolbar) nowLayout.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setTitle("關於我們");
        if (Build.VERSION.SDK_INT >= 21) {
            mToolbar.setPadding(0, GlobalConstant.Status_Bar_Height, 0, 0);
        }

        return nowLayout;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                Log.d(TAG, "home");
                ((MainActivity)mContext).onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item); // important line
    }
}
