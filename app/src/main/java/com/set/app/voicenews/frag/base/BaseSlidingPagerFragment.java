package com.set.app.voicenews.frag.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.set.app.voicenews.R;
import com.set.app.voicenews.util.DebugLog;
import com.set.app.voicenews.view.lib.slidingtab.SlidingTabLayout;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public abstract class BaseSlidingPagerFragment extends Fragment {
    private static final String TAG = "BaseSlidingPagerFragment";

    private Context mContext;
    protected Toolbar mToolbar;
    private View statusBGView;
    protected ProgressBar mProgressBar;
    protected RelativeLayout mRefreshRL;
    protected TextView mAlertTV;

    protected SlidingTabLayout mSlidingTabLayout;
    protected ViewPager mPager;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext        = context;
    }

    private View currentLayout=null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DebugLog.d(TAG, "onCreateView");
        View nowLayout = null;

        if(currentLayout == null){
            nowLayout      = inflater.inflate(R.layout.base_sliding_pager_hide_fragment_view, null);

//            setHasOptionsMenu(true);
            mToolbar        = (Toolbar) nowLayout.findViewById(R.id.toolbar);
            ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
            statusBGView = (View) nowLayout.findViewById(R.id.statusBGView);

            mRefreshRL          = (RelativeLayout) nowLayout.findViewById(R.id.refreshRL);
//            mRefreshRL.setOnClickListener(retryRequestListener);
            mAlertTV            = (TextView) nowLayout.findViewById(R.id.alertTV);
            mProgressBar 	    = (ProgressBar) nowLayout.findViewById(R.id.loadingProgressBar);

            mPager              = (ViewPager) nowLayout.findViewById(R.id.pager);
            mSlidingTabLayout   = (SlidingTabLayout)nowLayout.findViewById(R.id.indicator);

            setPager(mPager);
            setSlidingTab(mSlidingTabLayout);

            currentLayout = nowLayout;
            setRetryRequestListener(mRefreshRL);
            setStatusBGView(statusBGView);
            init();
        }else{
            DebugLog.d(TAG, "nowLayout != null");
            ViewGroup parent = (ViewGroup) currentLayout.getParent();
            if (parent != null){
                parent.removeView(currentLayout);
            }
            nowLayout = currentLayout;
            resumeFrag();
        }
        return nowLayout;
    }

    protected abstract void init();
    protected abstract void setStatusBGView(View statusBGView);
    protected abstract void resumeFrag();
    protected abstract void setRetryRequestListener(RelativeLayout mRefreshRL);
    protected abstract void setPager(ViewPager mPager);
    protected abstract void setSlidingTab(SlidingTabLayout mSlidingTabLayout);
}