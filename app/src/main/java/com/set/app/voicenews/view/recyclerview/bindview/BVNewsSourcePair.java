package com.set.app.voicenews.view.recyclerview.bindview;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.set.app.voicenews.R;
import com.set.app.voicenews.VoiceNewsApplication;
import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.util.prefs.SetNewsSourceUtil;
import com.set.app.voicenews.view.recyclerview.ViewTypeConstant;
import com.set.app.voicenews.view.recyclerview.ViewTypeInfo;
import com.set.app.voicenews.view.recyclerview.createview.CVGeneralList;

import java.util.List;

/**
 * Created by kennethyeh on 2017/1/9.
 */

public class BVNewsSourcePair {

    private Context mContext;
    private List<ViewTypeInfo> viewTypeList;

    private static int gridImgHeight = 0;
    private static final String ASSETS_PATH = "assets://newscover/%s.png";
    private SetNewsSourceUtil mSetNewsSourceUtil;

    public BVNewsSourcePair(Context mContext, List<ViewTypeInfo> viewTypeList){
        this.mContext       = mContext;
        this.viewTypeList   = viewTypeList;
        mSetNewsSourceUtil = SetNewsSourceUtil.getInstance(mContext);
    }

    public void setViewHolder(RecyclerView.ViewHolder viewHolder, int position){
        ViewTypeInfo viewTypeInfo = viewTypeList.get(position);
        switch (viewHolder.getItemViewType()) {

            case ViewTypeConstant.GeneralCellType.ITEM_GRID_PAIR_NEWS_SOURCE:

                CVGeneralList.HolderGridPair vhHolderGridPair  = (CVGeneralList.HolderGridPair) viewHolder;
                List<DataInfo.NewsSourceInfo> cellGridList      = (List<DataInfo.NewsSourceInfo>) viewTypeInfo.dataObject;

                View gridLeftRL         = vhHolderGridPair.gridLeftRL;
                ImageView leftIV        = vhHolderGridPair.leftIV;
                ImageView editLeftIV    = vhHolderGridPair.editLeftIV;

                View gridRightRL        = vhHolderGridPair.gridRightRL;
                ImageView rightIV       = vhHolderGridPair.rightIV;
                ImageView editRightIV   = vhHolderGridPair.editRightIV;

                gridRightRL.setVisibility(View.INVISIBLE);
                for(int i =0 ; i<cellGridList.size() ; i++){
                    DataInfo.NewsSourceInfo source = cellGridList.get(i);

                    if(i==0){ // left grid
                        String photoPath = String.format(ASSETS_PATH, source.id);
                        setImageViewHeight(leftIV);
                        VoiceNewsApplication.imageLoader.displayImage(photoPath,
                                leftIV,
                                options,
                                VoiceNewsApplication.animateFirstListener);

                        if(mSetNewsSourceUtil.getNewsSourcePref(source.id)){
                            editLeftIV.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_subs_checked));
                        }else{
                            editLeftIV.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_subs_add));
                        }
                        EditPrefInfo leftEdit =  new EditPrefInfo();
                        leftEdit.source = source;
                        leftEdit.checkIV = editLeftIV;
                        gridLeftRL.setTag(leftEdit);
                        gridLeftRL.setOnClickListener(editListener);// 預覽訂閱新聞列表

                    }else if(i==1){ // right grid
                        gridRightRL.setVisibility(View.VISIBLE);

                        String photoPath = String.format(ASSETS_PATH, source.id);
                        setImageViewHeight(rightIV);
                        VoiceNewsApplication.imageLoader.displayImage(photoPath,
                                rightIV,
                                options,
                                VoiceNewsApplication.animateFirstListener);

                        if(mSetNewsSourceUtil.getNewsSourcePref(source.id)){
                            editRightIV.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_subs_checked));
                        }else{
                            editRightIV.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_subs_add));
                        }
                        EditPrefInfo rightEdit =  new EditPrefInfo();
                        rightEdit.source = source;
                        rightEdit.checkIV = editRightIV;
                        gridRightRL.setTag(rightEdit);
                        gridRightRL.setOnClickListener(editListener);// 預覽訂閱新聞列表
                    }
                }

                break;

        }
    }

    private synchronized void setImageViewHeight(ImageView imageView){
        if(gridImgHeight!=0){
            ViewGroup.LayoutParams params = imageView.getLayoutParams();
            params.height = gridImgHeight;
            imageView.requestLayout();
        }else{
            final ImageView observerIV = imageView;
            ViewTreeObserver observer= observerIV.getViewTreeObserver();
            observer.addOnGlobalLayoutListener(
                    new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            int ivWidth = observerIV.getWidth();
                            ViewGroup.LayoutParams params = observerIV.getLayoutParams();
                            params.height = ivWidth;
                            gridImgHeight = params.height;
                            observerIV.requestLayout();
                            observerIV.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }
                    });
        }
    }

    public static DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.mipmap.loadingimg)
            .showImageForEmptyUri(R.mipmap.loadingimg)
            .showImageOnFail(R.mipmap.loadingimg).cacheInMemory(true)
            .cacheOnDisk(true).imageScaleType(ImageScaleType.EXACTLY)
//				.displayer(new RoundedBitmapDisplayer(20))
            .bitmapConfig(Bitmap.Config.RGB_565).build();


    class EditPrefInfo{
        DataInfo.NewsSourceInfo source ;
        ImageView checkIV;
    }
    private View.OnClickListener editListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            EditPrefInfo mEditPrefInfo = (EditPrefInfo) v.getTag();
            mSetNewsSourceUtil.toggleNewsSourcePref(mEditPrefInfo.source.id, mEditPrefInfo.checkIV);

        }
    };

}
