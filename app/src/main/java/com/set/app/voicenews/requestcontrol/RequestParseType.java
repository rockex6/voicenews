package com.set.app.voicenews.requestcontrol;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class RequestParseType {

    public enum ParseType {
        GetRequestToken
        ,PttNewsList
        ,FansNewsList
        ,NewsList
        ,NewsSource
        ,SearchNews
        ,SearchNewsResult
    }
}
