package com.set.app.voicenews.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.set.app.voicenews.dataInfo.DataInfo;

import org.json.JSONException;

import java.io.StringReader;

/**
 * Created by ResonTeng on 2017/1/10.
 */
public class ParserSearchNews {
    public DataInfo.SearchHotDO parsing(String value) throws JSONException {

        DataInfo.SearchHotDO mSearchHotDO = null;

        StringReader jsonSR  = new StringReader(value);

        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();

        JsonReader reader = new JsonReader(jsonSR);
        reader.setLenient(true);
        mSearchHotDO = gson.fromJson(reader, DataInfo.SearchHotDO.class);

        return mSearchHotDO;
    }
}
