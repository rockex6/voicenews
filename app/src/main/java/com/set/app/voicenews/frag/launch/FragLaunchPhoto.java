package com.set.app.voicenews.frag.launch;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.set.app.voicenews.R;
import com.set.app.voicenews.util.DebugLog;

/**
 * Created by kennethyeh on 16/4/27.
 */
public class FragLaunchPhoto extends Fragment {

    private static final String TAG = "FragLaunchPhoto";
    public static String FRAG_TRANS_NAME    = "FragLaunchPhoto";

    private Context mContext;
    private String name;

    public static FragLaunchPhoto newInstance() {

        FragLaunchPhoto frag = new FragLaunchPhoto();
        Bundle b = new Bundle();
        frag.setArguments(b);
        return frag;

    }

    public FragLaunchPhoto(){}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext        = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        DebugLog.d(TAG, "FragLaunchPhoto onCreateView");

        View nowLayout          = inflater.inflate(R.layout.frag_launch_photo, null);
        ImageView launchPhotoIV = (ImageView) nowLayout.findViewById(R.id.launchPhotoIV);
//        setLaucnPhoto(launchPhotoIV);
        return nowLayout;
    }

    private void setLaucnPhoto(ImageView launchPhotoIV){
//        CheckLaunchPhotoUtil.getInstance(mContext, launchPhotoIV).checkLaunchPhoto();//載入Launch圖
    }
}

