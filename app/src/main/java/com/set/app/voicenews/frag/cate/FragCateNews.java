package com.set.app.voicenews.frag.cate;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.set.app.voicenews.MainActivity;
import com.set.app.voicenews.R;
import com.set.app.voicenews.constant.GlobalConstant;
import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.frag.FragEditNewsSource;
import com.set.app.voicenews.frag.base.BaseSlidingPagerFragment;
import com.set.app.voicenews.frag.newslist.FragTabNewsList;
import com.set.app.voicenews.util.DebugLog;
import com.set.app.voicenews.util.prefs.SetNewsSourceUtil;
import com.set.app.voicenews.view.lib.slidingtab.SlidingTabLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class FragCateNews extends BaseSlidingPagerFragment {

    private static final String TAG = "FragCateNews";
    private Context mContext;
    private FragmentManager fragmentManager;
    private HashMap<Integer, Fragment> mFragMap = new HashMap<Integer, Fragment>();

    public static FragCateNews newInstance() {

        FragCateNews frag = new FragCateNews();
        Bundle b = new Bundle();
        frag.setArguments(b);
        return frag;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext        = context;
    }

    @Override
    protected void init() {
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    @Override
    protected void setStatusBGView(View statusBGView) {
        if (Build.VERSION.SDK_INT >= 21) {
            statusBGView.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams paramsStatusBG = (RelativeLayout.LayoutParams) statusBGView.getLayoutParams();
            paramsStatusBG.height = GlobalConstant.Status_Bar_Height;
            statusBGView.setLayoutParams(paramsStatusBG);
        }
    }

    @Override
    protected void resumeFrag() {

    }

    @Override
    protected void setRetryRequestListener(RelativeLayout mRefreshRL) {

    }

    @Override
    protected void setPager(ViewPager mPager) {


    }

    @Override
    protected void setSlidingTab(SlidingTabLayout mSlidingTabLayout) {

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(mContext.getResources().getString(R.string.cate_news));

        mSlidingTabLayout.setSameTabWidth(true);//設定tab平均螢幕等寬
        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                // TODO Auto-generated method stub
                return ContextCompat.getColor(mContext, R.color.tab_indicator_underline_color);
            }
            @Override
            public int getDividerColor(int position) {
                // TODO Auto-generated method stub
                return ContextCompat.getColor(mContext, android.R.color.transparent);
            }
        });


        List<DataInfo.NewsTabInfo> newsTabInfoList = new ArrayList<>();

        DataInfo.NewsTabInfo newsTabInfo1 = new DataInfo.NewsTabInfo();
        newsTabInfo1.category       = GlobalConstant.CateType.VOICE_NEWS_MULTI;
        newsTabInfo1.categoryStr    = GlobalConstant.NewsCategory.Category_NEWS;
        newsTabInfo1.orderType      = GlobalConstant.OrderType.ORDER_JUMPED;
        newsTabInfo1.name           = mContext.getResources().getString(R.string.news_jump);

        DataInfo.NewsTabInfo newsTabInfo2 = new DataInfo.NewsTabInfo();
        newsTabInfo2.category       = GlobalConstant.CateType.VOICE_NEWS_MULTI;
        newsTabInfo2.categoryStr    = GlobalConstant.NewsCategory.Category_NEWS;
        newsTabInfo2.orderType      = GlobalConstant.OrderType.ORDER_HOT;
        newsTabInfo2.name           = mContext.getResources().getString(R.string.news_hot);

        newsTabInfoList.add(newsTabInfo1);
        newsTabInfoList.add(newsTabInfo2);
        setTabPager(newsTabInfoList);
    }

    private void setTabPager(List<DataInfo.NewsTabInfo> newsTabInfoList){
        fragmentManager 	= getChildFragmentManager();
        DebugLog.d(TAG, "setTabPager");
        NormalTabListAdapter mTabListAdapter = new NormalTabListAdapter(fragmentManager, newsTabInfoList);
        mPager.setAdapter(mTabListAdapter);
        mSlidingTabLayout.setViewPager(mPager);
        mPager.setOffscreenPageLimit(1);
    }

    class NormalTabListAdapter extends FragmentStatePagerAdapter {

        private int originalSize=0;
        private List<DataInfo.NewsTabInfo> newsTabInfoList;

        public NormalTabListAdapter(FragmentManager fm) {
            super(fm);
        }

        public NormalTabListAdapter(FragmentManager fm, List<DataInfo.NewsTabInfo> newsTabInfoList) {
            super(fm);
            this.originalSize 	= newsTabInfoList.size();
            this.newsTabInfoList= newsTabInfoList;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment frag   = null;
            String title    = newsTabInfoList.get(position).name;
//            frag =  FragmentUnderConstruction.newInstance(title);

            DataInfo.NewsTabInfo mNewsTabInfo = newsTabInfoList.get(position);
            frag =  FragTabNewsList.newInstance(mNewsTabInfo);
            mFragMap.put(position, frag);
            return frag;
        }

        @Override
        public int getCount() {
            return originalSize;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title    = newsTabInfoList.get(position).name;
            return title;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            mFragMap.remove(position);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        DebugLog.i(TAG,"onCreateOptionsMenu");
        mToolbar.getMenu().clear();
        inflater.inflate(R.menu.menu_cate_news, mToolbar.getMenu());

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_edit) {
            DebugLog.d(TAG, "action_edit");

            FragEditNewsSource fragment  = FragEditNewsSource.newInstance();
            String FRAG_TRANS_NAME      = FragEditNewsSource.FRAG_TRANS_NAME;
            ((MainActivity)mContext).mFragRepTransUtil.replaceContentFragment(fragment, FRAG_TRANS_NAME);


            return true;
        }
        return super.onOptionsItemSelected(item); // important line
    }

    public void editChangeCheck(){
        DebugLog.d(TAG, "editChangeCheck");
        SetNewsSourceUtil mSetNewsSourceUtil = SetNewsSourceUtil.getInstance(mContext);
        if(!mSetNewsSourceUtil.getPrefEdit()){ // 無異動紀錄
            return;
        }
        mSetNewsSourceUtil.setPrefEdit(false);// reset edit stat to false
        for(Integer key :mFragMap.keySet()){
            FragTabNewsList frag = (FragTabNewsList) mFragMap.get(key);
            frag.editChangeCheck();
        }
    }
}
