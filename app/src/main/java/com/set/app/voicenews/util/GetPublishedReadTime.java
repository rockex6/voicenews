package com.set.app.voicenews.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by kennethyeh on 2017/1/4.
 */

public class GetPublishedReadTime {

    public static SimpleDateFormat sdfPublished = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public static SimpleDateFormat sdfRead = new SimpleDateFormat("yyyy.MM.dd");


    public synchronized static String getPublishDate(String dateString){
        String formatDate = "";
        try {
            Date date = sdfPublished.parse(dateString);
            formatDate = sdfRead.format(date.getTime());

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatDate;
    }
}
