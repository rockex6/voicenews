package com.set.app.voicenews;

import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;

import com.roughike.bottombar.BottomBar;
import com.set.app.voicenews.bottombarcontrol.BottomBarContainer;
import com.set.app.voicenews.constant.GlobalConstant;
import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.frag.FragmentUnderConstruction;
import com.set.app.voicenews.frag.cate.FragCateFansNews;
import com.set.app.voicenews.frag.cate.FragCateNews;
import com.set.app.voicenews.frag.cate.FragCatePttNews;
import com.set.app.voicenews.frag.cate.FragCateSearchNews;
import com.set.app.voicenews.frag.cate.FragCateSetting;
import com.set.app.voicenews.launch.ModelLaunch;
import com.set.app.voicenews.util.DebugLog;
import com.set.app.voicenews.util.FragReplaceTransUtil;
import com.set.app.voicenews.util.prefs.SetReqTokenUtil;
import com.set.app.voicenews.view.dialog.LeaveDialogControl;

import java.util.ArrayList;
import java.util.List;

import static com.set.app.voicenews.R.id.bottomBar;

public class MainActivity extends AppCompatActivity {

    private static final String TAG         = "MainActivity";
    private Activity mContext;

    public FragmentManager fragmentManager;
//    public RelativeLayout bottomBarRL;
    private BottomBar mBottomBar;
    private Fragment currentCateFrag = null;
    private BottomBarContainer mBottomBarContainer;

    private ModelLaunch mModelLaunch; // Launch工作類別
    public ProgressBar mProgressBar;
    private boolean launchFlag = true;

    private LeaveDialogControl mLeaveDialogControl = null;

    public FragReplaceTransUtil mFragRepTransUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        fragmentManager     = getSupportFragmentManager();
        mFragRepTransUtil   = new FragReplaceTransUtil(mContext, fragmentManager);

//        bottomBarRL        = (RelativeLayout) mContext.findViewById(R.id.bottomBarRL);
//        bottomBarRL.setVisibility(View.GONE);
        mBottomBarContainer = new BottomBarContainer(mContext);
        mProgressBar 	    = (ProgressBar) findViewById(R.id.loadingProgressBar);
        mLeaveDialogControl = LeaveDialogControl.getInstance(MainActivity.this);

        mBottomBar = (BottomBar) mContext.findViewById(bottomBar);
        showHideBottomBar(false);

        if (Build.VERSION.SDK_INT >= 21) {
            Window w = getWindow();
            // status bar 可自定半透明顏色solution
            w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            w.setStatusBarColor(ContextCompat.getColor(this, R.color.statusBarTransparent));
            // Fixes statusbar covers toolbar issue
            GlobalConstant.Status_Bar_Height = getStatusBarHeight();
        }
        startLaunch();
    }

    // A method to find height of the status bar
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void startLaunch(){
        mModelLaunch    = new ModelLaunch((MainActivity) mContext, mLaunchFinishListener);
        mModelLaunch.starLaunch();
    }

    /**
     * 完成Launch Callback
     * **/
    private ModelLaunch.LaunchFinishListener mLaunchFinishListener = new ModelLaunch.LaunchFinishListener(){
        @Override
        public void launchDone() {
            DebugLog.d(TAG, "@@ launchDone");
            mProgressBar.setVisibility(View.GONE);
            showBottomBar();
        }
    };

    private void showBottomBar(){
        DebugLog.d(TAG, "@@ launchDone");
        SetReqTokenUtil mSetReqTokenUtil = SetReqTokenUtil.getInstance(mContext);
        DataInfo.TokenInfo mTokenInfo = mSetReqTokenUtil.getReqTokenInfo();
        DebugLog.d(TAG, "token:"+mTokenInfo.token);
//        bottomBarRL.setVisibility(View.VISIBLE);

        List<DataInfo.BottomCateInfo> bottomCateInfoList = new ArrayList<>();
        DataInfo.BottomCateInfo c1 = new DataInfo.BottomCateInfo();
        c1.cateType = GlobalConstant.CateType.VOICE_PTT;
        c1.name     = getResources().getString(R.string.BottomBar_tab_ptt);
        bottomCateInfoList.add(c1);

        DataInfo.BottomCateInfo c2 = new DataInfo.BottomCateInfo();
        c2.cateType = GlobalConstant.CateType.VOICE_FANS_NEWS;
        c2.name     = getResources().getString(R.string.BottomBar_tab_news);
        bottomCateInfoList.add(c2);

        DataInfo.BottomCateInfo c3 = new DataInfo.BottomCateInfo();
        c3.cateType = GlobalConstant.CateType.VOICE_NEWS_MULTI;
        c3.name     = getResources().getString(R.string.BottomBar_tab_news_multi);
        bottomCateInfoList.add(c3);

        DataInfo.BottomCateInfo c4 = new DataInfo.BottomCateInfo();
        c4.cateType = GlobalConstant.CateType.VOICE_SEARCH;
        c4.name     = getResources().getString(R.string.BottomBar_tab_search);
        bottomCateInfoList.add(c4);

        DataInfo.BottomCateInfo c5 = new DataInfo.BottomCateInfo();
        c5.cateType = GlobalConstant.CateType.SETTING;
        c5.name     = getResources().getString(R.string.BottomBar_tab_setting);
        bottomCateInfoList.add(c5);


//        switchCateView(c1);
//        mBottomBarContainer.showBottomBar(bottomCateInfoList);
        mBottomBarContainer.selectBottomBar(bottomCateInfoList);
    }

    public void switchCateView(DataInfo.BottomCateInfo bottomCateInfo){
        Fragment frag = FragmentUnderConstruction.newInstance("voice");
        int cateType = bottomCateInfo.cateType;

        DebugLog.d(TAG,"cateType:"+bottomCateInfo.cateType);
        DebugLog.d(TAG,"name:"+bottomCateInfo.name);
        switch(cateType){
            case GlobalConstant.CateType.VOICE_PTT:
                frag = FragCatePttNews.newInstance();
                break;
            case GlobalConstant.CateType.VOICE_FANS_NEWS:
                frag = FragCateFansNews.newInstance();
                break;
            case GlobalConstant.CateType.VOICE_NEWS_MULTI:
                frag = FragCateNews.newInstance();
                break;
            case GlobalConstant.CateType.VOICE_SEARCH:
                frag = FragCateSearchNews.newInstance();
                break;
            case GlobalConstant.CateType.SETTING:
                frag = FragCateSetting.newInstance();
                break;
        }

        gotoCateFrag(frag);
    }

    private void gotoCateFrag(Fragment frag){
        currentCateFrag = frag;
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.categoryFrag, frag);
        transaction.commit();
        new Handler().postDelayed(new Runnable() { @Override public void run() { launchFlag = false; } }, 200);
    }

    public void removeCateFrag(){
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.remove(currentCateFrag);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {

        int count = fragmentManager.getBackStackEntryCount();
        DebugLog.i(TAG, "BACK STACK COUNT:" + count);
        if(count==0){

            showDialog();

        }else if(count ==1){
            DebugLog.d(TAG, "onBackPressed backToMain");
            showHideBottomBar(true);

            if(currentCateFrag instanceof FragCateNews){
                DebugLog.d(TAG, "backToMain FragCateNews");
                ((FragCateNews)currentCateFrag).editChangeCheck();
            }

            if(currentCateFrag instanceof FragCateSearchNews){
                gotoCateFrag(FragCateSearchNews.newInstance());
            }


            //返回最外層MainActivity
            super.onBackPressed();
        }else{
            super.onBackPressed();
        }
    }

    public void showHideBottomBar(boolean show){
        if(show){
            mBottomBar.setVisibility(View.VISIBLE);
        }else{
            mBottomBar.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(launchFlag){return;}
    }

    private void showDialog(){
//        int type = LeaveDialogControl.DIALOG_TYPE_NORMAL;
        int type = LeaveDialogControl.DIALOG_TYPE_NORMAL;
        Dialog dialog = mLeaveDialogControl.getDialog(type);
        dialog.show();
    }
}
