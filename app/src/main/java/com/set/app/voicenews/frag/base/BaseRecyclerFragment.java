package com.set.app.voicenews.frag.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.set.app.voicenews.R;
import com.set.app.voicenews.util.DebugLog;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public abstract class BaseRecyclerFragment extends Fragment {
    private static final String TAG = "BaseRecyclerFragment";

    private Context mContext;
    protected RecyclerView mRecyclerView;
    //    private RecyclerViewAdapter mRecyclerViewAdapter;
    protected SwipeRefreshLayout mSwipeRefreshLayout;
    //    private RelativeLayout mListLoadmoreRL;
    protected LinearLayoutManager layoutManager;

    /* 更新提示layout */
    private View mUpdateHintView;
    protected RelativeLayout mUpdateHintRL;

    protected ProgressBar mProgressBar;

    public static final int LIST_INIT 		= 0;
    public static final int LIST_LOAD_MORE 	= 1;
    public static final int LIST_REFRESH 	= 2;

    protected RelativeLayout mRefreshRL;
    protected TextView mAlertTV;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext        = context;
    }

    private View currentLayout=null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DebugLog.d(TAG, "onCreateView");
        View nowLayout = null;

        if(currentLayout == null){
            nowLayout      = inflater.inflate(R.layout.base_refresh_recycler_view, null);

            mRefreshRL          = (RelativeLayout) nowLayout.findViewById(R.id.refreshRL);
//            mRefreshRL.setOnClickListener(retryRequestListener);
            mAlertTV            = (TextView) nowLayout.findViewById(R.id.alertTV);

            mRecyclerView       = (RecyclerView) nowLayout.findViewById(R.id.recyclerView);

            layoutManager = new LinearLayoutManager(mContext);
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            mRecyclerView.setLayoutManager(layoutManager);
            mSwipeRefreshLayout = (SwipeRefreshLayout) nowLayout.findViewById(R.id.swipeRefreshLayout);
            //        mSwipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);
            //        mSwipeRefreshLayout.setColorSchemeResources(R.color.app_theme_color_dark, R.color.app_theme_color, R.color.app_theme_color_dark);

            //        mListLoadmoreRL     = (RelativeLayout) nowLayout.findViewById(R.id.listLoadmoreRL);
            mProgressBar 	    = (ProgressBar) nowLayout.findViewById(R.id.loadingProgressBar);


            mUpdateHintView = nowLayout.findViewById(R.id.updateHintView);
            mUpdateHintRL   = (RelativeLayout) mUpdateHintView.findViewById(R.id.updateHintRL);

            currentLayout = nowLayout;
            init();
            setRetryRequestListener(mRefreshRL);
            setOnRefreshListener(mSwipeRefreshLayout);
            setOnScrollListener(mRecyclerView);
        }else{
            DebugLog.d(TAG, "nowLayout != null");
            ViewGroup parent = (ViewGroup) currentLayout.getParent();
            if (parent != null){
                parent.removeView(currentLayout);
            }
            nowLayout = currentLayout;
            resumeFrag();
        }
        return nowLayout;
    }

    protected abstract void init();
    protected abstract void resumeFrag();
    protected abstract void setRetryRequestListener(RelativeLayout mRefreshRL);
    protected abstract void setOnRefreshListener(SwipeRefreshLayout mSwipeRefreshLayout);
    protected abstract void setOnScrollListener(RecyclerView mRecyclerView);
}
