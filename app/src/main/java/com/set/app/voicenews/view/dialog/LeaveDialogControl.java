package com.set.app.voicenews.view.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.set.app.voicenews.MainActivity;
import com.set.app.voicenews.R;

/**
 * Created by kennethyeh on 2017/1/9.
 */

public class LeaveDialogControl {

    private MainActivity mContext;

    public static final int DIALOG_TYPE_NORMAL  = 1;

    public static LeaveDialogControl getInstance(MainActivity context){
        return new LeaveDialogControl(context);
    }
    private LeaveDialogControl(MainActivity mContext){
        this.mContext = mContext;
    }

    public Dialog getDialog(int type){
        Dialog mDialog = null;
        switch(type){
            case DIALOG_TYPE_NORMAL:
                mDialog = getDialogNormal();
                break;

            default:
                mDialog = getDialogNormal();
                break;
        }

        mDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
//                GATracker.getInstance(mContext).trackEvent(GATracker.Category_Leave_App, gaAction_Leave_App_Other, gaLabel_Leave_App_Cancel, null);
            }
        });

        return mDialog;
    }

    private Dialog getDialogNormal(){

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        String AppNameDisplay = mContext.getResources().getString(R.string.app_name);
        builder.setMessage("確定離開"+AppNameDisplay+"？");
        builder.setPositiveButton("確定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
//                GATracker.getInstance(mContext).trackEvent(GATracker.Category_Leave_App, gaAction_Leave_App_Other, gaLabel_Leave_App_Normal_Leave, null);
//                mContext.leaveApp();
                mContext.finish();
            }
        });
        builder.setNegativeButton("取消",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
//                GATracker.getInstance(mContext).trackEvent(GATracker.Category_Leave_App, gaAction_Leave_App_Other, gaLabel_Leave_App_Normal_Stay, null);
                dialog.dismiss();
            }
        });
        Dialog mDialog = builder.create();

        return mDialog;
    }
}
