package com.set.app.voicenews.frag.cate;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.set.app.voicenews.BuildConfig;
import com.set.app.voicenews.MainActivity;
import com.set.app.voicenews.R;
import com.set.app.voicenews.constant.APIConstant;
import com.set.app.voicenews.constant.GlobalConstant;
import com.set.app.voicenews.frag.setting.FragAboutMe;
import com.set.app.voicenews.util.DebugLog;

/**
 * Created by ResonTeng on 2017/1/9.
 */
public class FragCateSetting extends Fragment {
    private static final String TAG             = "FragCateSetting";
    private Context mContext;

    private Toolbar mToolbar;
    private RelativeLayout SetRating, SetAbout, shareApp;
    private TextView AppVersion;

    public static FragCateSetting newInstance() {
        FragCateSetting frag = new FragCateSetting();
        Bundle b = new Bundle();
        frag.setArguments(b);
        return frag;
    }

    public FragCateSetting(){}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DebugLog.d(TAG, TAG + " onCreateView");

        View nowLayout  = inflater.inflate(R.layout.frag_cate_setting, null);
        setHasOptionsMenu(true);
        mToolbar        = (Toolbar) nowLayout.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setTitle("更多");
        if (Build.VERSION.SDK_INT >= 21) {
            mToolbar.setPadding(0, GlobalConstant.Status_Bar_Height, 0, 0);
        }

        SetRating   = (RelativeLayout) nowLayout.findViewById(R.id.set_rating);
        shareApp    = (RelativeLayout) nowLayout.findViewById(R.id.share_app);
        SetAbout    = (RelativeLayout) nowLayout.findViewById(R.id.set_about);
        AppVersion  = (TextView) nowLayout.findViewById(R.id.app_version);

        SetRating.setOnClickListener(mClickListener);
        shareApp.setOnClickListener(mClickListener);
        SetAbout.setOnClickListener(mClickListener);

        try {
            PackageManager pm = mContext.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(mContext.getPackageName(), PackageManager.GET_ACTIVITIES);

            if (pi != null) {
                AppVersion.setText(pi.versionName == null ? "" : "v."+pi.versionName);
                if(BuildConfig.DEBUG){
                    AppVersion.setText(pi.versionName == null ? "" : "Develop v."+pi.versionName);
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "an error occured when collect package info", e);
        }

        return nowLayout;
    }

    private View.OnClickListener mClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if(clickFlag){return;}clickFlag = true;
            clickHandler.postDelayed(clickRunnable, Clickable_Millis_Time);

            switch(id){
                case R.id.set_rating:
                    String ratinglink = APIConstant.URL_PLAY_APP; //+"#details-reviews";//會導到Web但卻沒評價選項
                    Intent i = new Intent();
                    i.setAction(Intent.ACTION_VIEW);
                    i.addCategory(Intent.CATEGORY_BROWSABLE);
                    i.setData(Uri.parse(ratinglink));
                    mContext.startActivity(i);
                    break;
                case R.id.share_app:
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    //sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "分享App");
                    String AppNameDisplay = getResources().getString(R.string.app_name);
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, AppNameDisplay + "App | " + APIConstant.shareAppURL);
                    startActivity(Intent.createChooser(sharingIntent, "分享連結"));
                    break;
                case R.id.set_about:
                    FragAboutMe mFragAboutMe  = FragAboutMe.newInstance();
                    ((MainActivity)mContext).mFragRepTransUtil.replaceContentFragment(mFragAboutMe, mFragAboutMe.FRAG_TRANS_NAME);
                    break;
            }
        }
    };

    private boolean clickFlag = false;                 // 點擊狀態
    private final int Clickable_Millis_Time = 1500;     // 控制可再次點擊delay秒數
    private Handler clickHandler = new Handler();       // 控制點擊頻率Handler
    private Runnable clickRunnable = new Runnable(){
        @Override
        public void run() {
            clickHandler.removeCallbacks(clickRunnable);
            clickFlag = false;
        }
    };

}
