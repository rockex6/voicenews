package com.set.app.voicenews.constant;

import com.set.app.voicenews.BuildConfig;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class APIConstant {
    //    public static final boolean isDebug = false;
    public static final boolean isDebug = BuildConfig.DEBUG;

    public static String GET_REQ_TOKEN = getSeriverSite()+"api/get-token?user=sanlih.pms@gmail.com";

    public static String API_Get_Articles_Order_Type    = getSeriverSite()+"api/get-articles?category=%s&order-type=%s&token=%s";
    public static String API_Get_Articles_Order_Base    = getSeriverSite()+"api/get-articles?category=%s&order-base=%s&token=%s";

    public static String API_Get_News_Source    = getSeriverSite()+"api/get-sources?category=news&token=%s";

    public static String API_SEARCH_HOT         = "http://appv2.settv-it.com/public/appServices/keywordPop_VoiceNews.json";
    public static String API_Search_Articles    = getSeriverSite()+"api/search-articles?keyword=%s&category=%s&page-wanted=%s&token=%s";

    public static String URL_PLAY_APP           = "https://play.google.com/store/apps/details?id=com.set.app.voicenews";
    public static String shareAppURL            = "http://voice.settv-it.com/";

    public static String ETtoday_News_API       = "http://www.ettoday.net/data/app/app_block/api/news/android-news-2016.php?news_id=%s";

    public static String getSeriverSite() {
        if (isDebug) {
            return "http://voice.settv-it.com/";
        } else {
            return "http://voice.settv-it.com/";

        }
    }
}
