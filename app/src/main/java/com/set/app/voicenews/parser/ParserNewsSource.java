package com.set.app.voicenews.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.set.app.voicenews.dataInfo.DataInfo;

import org.json.JSONException;

import java.io.StringReader;

/**
 * Created by kennethyeh on 2017/1/9.
 */

public class ParserNewsSource {
    public DataInfo.NewsSource parsing(String value) throws JSONException {

        DataInfo.NewsSource mNewsSource = null;

        StringReader jsonSR  = new StringReader(value);

        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();

        JsonReader reader = new JsonReader(jsonSR);
        reader.setLenient(true);
//        Type collectionType = new TypeToken<CateListMix>(){}.getType();
        mNewsSource = gson.fromJson(reader, DataInfo.NewsSource.class);

        return mNewsSource;
    }
}
