package com.set.app.voicenews.dataInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class DataInfo {

    public static class ReqTokenInfo implements Serializable {
        public TokenInfo data;
        public boolean success;
    }

    public static class TokenInfo implements Serializable{
        public String   issued="";
        public String   token="";
        public String   expiration="";
    }


    public static class BottomCateInfo implements Serializable{
        public int cateType;
        public String name;
    }

    public static class NewsTabInfo implements Serializable{
        public int category;
        public String categoryStr;
//        public int orderBase;
        public String orderBase;
//        public int orderType;
        public String orderType;
        public String name;
    }

    public static class PttNewsList implements Serializable {
        public List<PttNewsDO> data;
        public boolean success;
    }

    public static class PttNewsDO implements Serializable {
        public String   source;
        public String   source_name="";
        public String   rate_boo;
        public int num_ptters;
        public int delta_ptters;
        public String   title;
        public String   url;
        public String   rate_push;
        public String   author;
        public String   published;
    }

    public static class NewsList implements Serializable {
        public List<NewsDO> data;
        public boolean success;
    }

    public static class NewsDO implements Serializable {
        public int delta_reactors;
        public String source_name="";
        public String   title;
        public String   url;
//        public String   source;
        public int num_reactors;
        public String   published;
        public String source_id="";

        // fb field
        public int num_likes;
        public int num_comments;
        public int num_shares;
    }

    public static class NewsSource implements Serializable {
        public List<NewsSourceInfo> data = new ArrayList<>();
        public boolean success;
    }
    public static class NewsSourceInfo implements Serializable {
        public String id="";
        public String name="";
    }


    /* SearchNews */
    public static class DescTitleDO implements Serializable {
        public String describeTitleStr;
        public boolean showDivider = false;
    }
    public static class SearchRecordDO implements Serializable {
        public String keyword;
        public boolean showRemoveIcon = false;
    }
    /* 熱門搜尋 */
    public static class SearchHotDO implements Serializable{
        public List<String> keywordPop = new ArrayList<String>();
    }

    public static class SearchDO implements Serializable {
        public SearchNewsDO data;
        public boolean success;
    }
    public static class SearchNewsDO implements Serializable {
        public List<NewsDO> articles;
        public int current_page;
        public int total_pages;
    }
}
