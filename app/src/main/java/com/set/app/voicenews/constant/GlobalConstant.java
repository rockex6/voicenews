package com.set.app.voicenews.constant;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class GlobalConstant {

    public static int Status_Bar_Height = 0;
    public static int Screen_Width = 0;

    public interface CateType{
        int VOICE_PTT           = 1; //
        int VOICE_FANS_NEWS     = 2; //
        int VOICE_NEWS_MULTI    = 3; //
        int VOICE_SEARCH        = 4; //
        int SETTING             = 5; //
    }

    public interface NewsCategory{
        String Category_PTT         = "ptt";
        String Category_FANS_NEWS   = "facebook";
        String Category_NEWS        = "news";
    }

    public interface OrderBase{
        String ORDER_BASE_LIKES     = "likes";
        String ORDER_BASE_SHARES    = "shares";
        String ORDER_BASE_COMMENTS  = "comments";
    }

    public interface OrderType{
        String ORDER_JUMPED       = "jumped";
        String ORDER_HOT          = "hot";
    }
}
