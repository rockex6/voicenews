package com.set.app.voicenews.view.recyclerview;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class ViewTypeConstant {

    public  static class GeneralCellType {
        public static final int ITEM_PTT_NEWS               = 0;
        public static final int ITEM_FANS_NEWS              = 1;
        public static final int ITEM_NEWS                   = 2;
        public static final int ITEM_GRID_PAIR_NEWS_SOURCE  = 3;
        public static final int ITEM_LOADING                = 4;
        public static final int ITEM_DESCRIPTION_TITIE      = 5;
        public static final int ITEM_SEARCH_RECORD_CONTENT  = 6;
    }
}

