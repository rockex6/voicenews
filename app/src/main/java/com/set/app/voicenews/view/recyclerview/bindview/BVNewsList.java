package com.set.app.voicenews.view.recyclerview.bindview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.set.app.voicenews.R;
import com.set.app.voicenews.VoiceNewsApplication;
import com.set.app.voicenews.WebviewPagerActivity;
import com.set.app.voicenews.constant.GlobalConstant;
import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.util.GetPublishedReadTime;
import com.set.app.voicenews.util.NumberToReadFormat;
import com.set.app.voicenews.view.recyclerview.ViewTypeConstant;
import com.set.app.voicenews.view.recyclerview.ViewTypeInfo;
import com.set.app.voicenews.view.recyclerview.createview.CVGeneralList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class BVNewsList {

    private static final String TAG ="BVActionList";

    private Context mContext;
    private List<ViewTypeInfo> viewTypeList;
    private static int newsImgHeightSmall = 0;

    private DataInfo.NewsTabInfo mNewsTabInfo = null;

    private boolean isSearchNewsResult = false;

    private List<Object> mNewsList = new ArrayList<>();

    private static final String ASSETS_PATH = "assets://newscover/%s.png";
    public static DisplayImageOptions options = new DisplayImageOptions.Builder()
            .showImageOnLoading(R.mipmap.loadingimg)
            .showImageForEmptyUri(R.mipmap.loadingimg)
            .showImageOnFail(R.mipmap.loadingimg).cacheInMemory(true)
            .cacheOnDisk(true).imageScaleType(ImageScaleType.EXACTLY)
//				.displayer(new RoundedBitmapDisplayer(20))
            .bitmapConfig(Bitmap.Config.RGB_565).build();

    public BVNewsList(Context mContext, List<ViewTypeInfo> viewTypeList){
        this.mContext       = mContext;
        this.viewTypeList   = viewTypeList;
    }

    public BVNewsList(Context mContext, List<ViewTypeInfo> viewTypeList, boolean isSearchNewsResult){
        this.mContext       = mContext;
        this.viewTypeList   = viewTypeList;
        this.isSearchNewsResult = isSearchNewsResult;
    }

    public void setNewsTabInfo(DataInfo.NewsTabInfo newsTabInfo){
        this.mNewsTabInfo = newsTabInfo;
    }

    public void setNewsList(List<Object> newsList){
        mNewsList = newsList;
    }

    private int lastPosition = -1;
    public void setViewHolder(RecyclerView.ViewHolder viewHolder, int position){
        ViewTypeInfo viewTypeInfo = viewTypeList.get(position);

        switch (viewHolder.getItemViewType()) {

            case ViewTypeConstant.GeneralCellType.ITEM_PTT_NEWS:
                CVGeneralList.HolderPttNews vhHolderPttNews  = (CVGeneralList.HolderPttNews) viewHolder;
                DataInfo.PttNewsDO mPttNewsDO    = (DataInfo.PttNewsDO) viewTypeInfo.dataObject;
                RelativeLayout contentRL    = vhHolderPttNews.contentRL;
                TextView sourceTV           = vhHolderPttNews.sourceTV;
                TextView dateTV             = vhHolderPttNews.dateTV;
                TextView titleTV            = vhHolderPttNews.titleTV;
                TextView countTV            = vhHolderPttNews.countTV;

                sourceTV.setText(mPttNewsDO.source_name);
                String published =GetPublishedReadTime.getPublishDate(mPttNewsDO.published);
                dateTV.setText(published);
                titleTV.setText(mPttNewsDO.title);

                String num_ptters = NumberToReadFormat.getConvertFormat(mPttNewsDO.num_ptters);
                countTV.setText(num_ptters);

                contentRL.setTag(mPttNewsDO);
                contentRL.setOnClickListener(clickPttNewsListener);
                break;

            case ViewTypeConstant.GeneralCellType.ITEM_FANS_NEWS:
                CVGeneralList.HolderFansNews vhHolderFansNews  = (CVGeneralList.HolderFansNews) viewHolder;
                DataInfo.NewsDO mFansNewsDO    = (DataInfo.NewsDO) viewTypeInfo.dataObject;
                RelativeLayout contentFansRL    = vhHolderFansNews.contentRL;
                TextView sourceFansTV           = vhHolderFansNews.sourceTV;
                TextView dateFansTV             = vhHolderFansNews.dateTV;
                TextView titleFansTV            = vhHolderFansNews.titleTV;
                ImageView countIV               = vhHolderFansNews.countIV;
                TextView countFansTV            = vhHolderFansNews.countTV;
                int fansCount=0;

                sourceFansTV.setText(mFansNewsDO.source_name);
                String publishedFans =GetPublishedReadTime.getPublishDate(mFansNewsDO.published);
                dateFansTV.setText(publishedFans);

                String fansTitle = ((mFansNewsDO.title==null)||(mFansNewsDO.title.trim().equals("")))
                        ?(mContext.getResources().getString(R.string.fans_news_no_title))
                        :(mFansNewsDO.title);
                titleFansTV.setText(fansTitle);
                if(mNewsTabInfo!=null){
                    String orderBase = mNewsTabInfo.orderBase;
                    if(orderBase.equalsIgnoreCase(GlobalConstant.OrderBase.ORDER_BASE_LIKES)){
                        fansCount = mFansNewsDO.num_likes;
                        countIV.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_thumbsup));
                    }
                    if(orderBase.equalsIgnoreCase(GlobalConstant.OrderBase.ORDER_BASE_SHARES)){
                        fansCount = mFansNewsDO.num_shares;
                        countIV.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_share_w));
                    }
                    if(orderBase.equalsIgnoreCase(GlobalConstant.OrderBase.ORDER_BASE_COMMENTS)){
                        fansCount = mFansNewsDO.num_comments;
                        countIV.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_message));
                    }
                }
                String numCountStr = NumberToReadFormat.getConvertFormat(fansCount);
                countFansTV.setText(numCountStr);

                contentFansRL.setTag(mFansNewsDO);
                contentFansRL.setOnClickListener(clickFansNewsListener);
                break;

            case ViewTypeConstant.GeneralCellType.ITEM_NEWS:
                CVGeneralList.HolderNews vhHolderNews  = (CVGeneralList.HolderNews) viewHolder;
                DataInfo.NewsDO mNewsDO    = (DataInfo.NewsDO) viewTypeInfo.dataObject;
                RelativeLayout contentNewsRL    = vhHolderNews.contentRL;
                ImageView sourceIV              = vhHolderNews.sourceIV;
                TextView sourceNewsTV           = vhHolderNews.sourceTV;
                TextView dateNewsTV             = vhHolderNews.dateTV;
                TextView titleNewsTV            = vhHolderNews.titleTV;
                TextView countNewsTV            = vhHolderNews.countTV;

                String photoPath = String.format(ASSETS_PATH, mNewsDO.source_id);
                VoiceNewsApplication.imageLoader.displayImage(photoPath,
                        sourceIV,
                        options,
                        VoiceNewsApplication.animateFirstListener);

                sourceNewsTV.setText(mNewsDO.source_name);
                String publishedNews =GetPublishedReadTime.getPublishDate(mNewsDO.published);
                dateNewsTV.setText(publishedNews);
                titleNewsTV.setText(mNewsDO.title);

                String num_reactors = NumberToReadFormat.getConvertFormat(mNewsDO.num_reactors);
                countNewsTV.setText(num_reactors);

                contentNewsRL.setTag(mNewsDO);
                contentNewsRL.setOnClickListener(clickNewsListener);
                break;
        }
        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
//        Animation animationBounce = AnimationUtils.loadAnimation(mContext,
//                (position > lastPosition) ? R.anim.up_from_bottom_bounce : R.anim.down_from_top_bounce);
        viewHolder.itemView.startAnimation(animation);
        lastPosition = position;
    }


    private View.OnClickListener clickPttNewsListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DataInfo.PttNewsDO mPttNewsDO = (DataInfo.PttNewsDO) view.getTag();
            String webviewTitle = mContext.getResources().getString(R.string.cate_ptt);
            openWebviewPager(webviewTitle, mPttNewsDO);

//            Intent intent = new Intent();
//            intent.setClass(mContext, WebviewActivity.class);
//            intent.putExtra(WebviewActivity.Para_key_Type, WebviewActivity.TYPE_MORMAL);
////            String webviewTitle = mPttNewsDO.title;
//            intent.putExtra(WebviewActivity.Para_key_Title,webviewTitle);
//            intent.putExtra("url", mPttNewsDO.url);
//            mContext.startActivity(intent);
        }
    };

    private View.OnClickListener clickFansNewsListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DataInfo.NewsDO mNewsDO = (DataInfo.NewsDO) view.getTag();
            String webviewTitle = mContext.getResources().getString(R.string.cate_fans_news);
            openWebviewPager(webviewTitle, mNewsDO);

//            Intent intent = new Intent();
//            intent.setClass(mContext, WebviewActivity.class);
//            intent.putExtra(WebviewActivity.Para_key_Type, WebviewActivity.TYPE_MORMAL);
////            String webviewTitle = mNewsDO.title;
//            intent.putExtra(WebviewActivity.Para_key_Title,webviewTitle);
//            intent.putExtra("url", mNewsDO.url);
//            mContext.startActivity(intent);
        }
    };

    private View.OnClickListener clickNewsListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            DataInfo.NewsDO mNewsDO = (DataInfo.NewsDO) view.getTag();
            String webviewTitle = mContext.getResources().getString(R.string.cate_news);
            if(isSearchNewsResult){
                webviewTitle = mContext.getResources().getString(R.string.search_news_result_title);
            };
            openWebviewPager(webviewTitle, mNewsDO);

//            Intent intent = new Intent();
//            intent.setClass(mContext, WebviewActivity.class);
//            intent.putExtra(WebviewActivity.Para_key_Type, WebviewActivity.TYPE_MORMAL);
////            String webviewTitle = mNewsDO.title;
//            intent.putExtra(WebviewActivity.Para_key_Title,webviewTitle);
//            intent.putExtra("url", mNewsDO.url);
//            mContext.startActivity(intent);
        }
    };

    private void openWebviewPager(String title, Object newsData){
        Intent intent = new Intent();
        intent.setClass(mContext, WebviewPagerActivity.class);
        intent.putExtra(WebviewPagerActivity.Para_key_Title, title);
        intent.putExtra(WebviewPagerActivity.Para_key_Position_in_List, trackPosition(newsData));
        intent.putExtra(WebviewPagerActivity.Para_key_NewsList, (Serializable)mNewsList);
        mContext.startActivity(intent);
    }

    private int trackPosition(Object newsData){
        int position=0;
        if(mNewsList.size()==0){
            mNewsList.add(newsData);
            return position;
        }
        if(newsData instanceof DataInfo.NewsDO){
            DataInfo.NewsDO currentNews = (DataInfo.NewsDO) newsData;
            String currentIndex = currentNews.source_id+"_"+currentNews.url;
            for(int i =0 ; i<mNewsList.size() ; i++){
                DataInfo.NewsDO mNewsDO = (DataInfo.NewsDO) mNewsList.get(i);
                String chkIndex = mNewsDO.source_id+"_"+mNewsDO.url;
                if(currentIndex.equals(chkIndex)){
                    position = i;
                    break;
                }
            }
        }

        if(newsData instanceof DataInfo.PttNewsDO){
            DataInfo.PttNewsDO currentNews = (DataInfo.PttNewsDO) newsData;
            String currentIndex = currentNews.source_name+"_"+currentNews.url;
            for(int i =0 ; i<mNewsList.size() ; i++){
                DataInfo.PttNewsDO mNewsDO = (DataInfo.PttNewsDO) mNewsList.get(i);
                String chkIndex = mNewsDO.source_name+"_"+mNewsDO.url;
                if(currentIndex.equals(chkIndex)){
                    position = i;
                    break;
                }
            }
        }

        return position;
    }
}
