package com.set.app.voicenews.bottombarcontrol;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;
import com.set.app.voicenews.MainActivity;
import com.set.app.voicenews.R;
import com.set.app.voicenews.constant.GlobalConstant;
import com.set.app.voicenews.dataInfo.DataInfo;

import java.util.ArrayList;
import java.util.List;

import static com.set.app.voicenews.R.id.bottomBar;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class BottomBarContainer {

    private static final String TAG = "BottomBarContainer";

    private Activity mContext;
    private String backgroundColor=" ";
    private String fontColor=" ";
    private String lineColor=" ";

    private LinearLayout mBottomBarLL;

    private int clickedCateType ;
    private List<BottomNaviHolder> bottomNaviList= new ArrayList<BottomNaviHolder>();
    public class BottomNaviHolder{
        public int cateType;
        public ImageView icon;
        public TextView name;
        public int Res_Icon     ;
        public int Res_Icon_Select;
    }

    public BottomBarContainer(Activity mContext){
        this.mContext = mContext;
    }

    public void showBottomBar(List<DataInfo.BottomCateInfo> bottomCateInfoList){
//        mBottomBarLL    = (LinearLayout) mContext.findViewById(R.id.bottomBarLL);
        mBottomBarLL.removeAllViews();

        int size = bottomCateInfoList.size();
        for(int i =0 ; i<size ; i++){
            DataInfo.BottomCateInfo bottomCateInfo = bottomCateInfoList.get(i);
            View bottomItemView = mContext.getLayoutInflater().inflate(R.layout.item_bottom_bar, null);
            RelativeLayout bottomBarItemParentRL    = (RelativeLayout) bottomItemView.findViewById(R.id.bottomBarItemParentRL);
            RelativeLayout bottomBarItemRL          = (RelativeLayout) bottomItemView.findViewById(R.id.bottomBarItemRL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
            params.weight = 1.0f;
            bottomBarItemParentRL.setLayoutParams(params);
            View divideView = bottomItemView.findViewById(R.id.divideView);

            TextView bottomBarItemTV = (TextView) bottomItemView.findViewById(R.id.bottomBarItemTV);
            String name = bottomCateInfo.name;
            bottomBarItemTV.setText(name);

            BottomNaviHolder holder = new BottomNaviHolder();
            holder.cateType = bottomCateInfo.cateType;
            holder.name     = bottomBarItemTV;
            bottomNaviList.add(holder);

            if(i==(size-1)){
                divideView.setVisibility(View.GONE);
            }
//
            bottomBarItemRL.setTag(bottomCateInfo);
            bottomBarItemRL.setOnClickListener(openClickListener);
//
            mBottomBarLL.addView(bottomItemView);
        }
        setClickCateType(GlobalConstant.CateType.VOICE_PTT);
    }

    public void setClickCateType(int cateType){
        clickedCateType = cateType;
        resetBottomBarUI();
    }

    private View.OnClickListener openClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DataInfo.BottomCateInfo bottomCateInfo = (DataInfo.BottomCateInfo) v.getTag();
            if(bottomCateInfo.cateType == clickedCateType){
                return;
            }
            setClickCateType(bottomCateInfo.cateType);
            ((MainActivity)mContext).switchCateView(bottomCateInfo);
        }
    };

    public void selectBottomBar(final List<DataInfo.BottomCateInfo> bottomCateInfoList){
        BottomBar mBottomBar = (BottomBar) mContext.findViewById(bottomBar);

        mBottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                int size = bottomCateInfoList.size();
                for(int i =0 ; i<size ; i++){
                    String clickTabName = getTabMessage(tabId, false);
                    DataInfo.BottomCateInfo bottomCateInfo = bottomCateInfoList.get(i);
                    if(clickTabName.equalsIgnoreCase(bottomCateInfo.name)){
                        ((MainActivity)mContext).switchCateView(bottomCateInfo);
                        return;
                    }
                }
            }
        });

        mBottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
//                Toast.makeText(mContext.getApplicationContext(), getTabMessage(tabId, true), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String getTabMessage(int menuItemId, boolean isReselection) {
        String message = "";

        switch (menuItemId) {
            case R.id.tab_ptt:
                message = mContext.getResources().getString(R.string.BottomBar_tab_ptt);
                break;
            case R.id.tab_news:
                message = mContext.getResources().getString(R.string.BottomBar_tab_news);
                break;
            case R.id.tab_news_multi:
                message = mContext.getResources().getString(R.string.BottomBar_tab_news_multi);
                break;
            case R.id.tab_search:
                message = mContext.getResources().getString(R.string.BottomBar_tab_search);
                break;
            case R.id.tab_setting:
                message = mContext.getResources().getString(R.string.BottomBar_tab_setting);
                break;
        }

        if (isReselection) {
            message += " WAS RESELECTED!";
        }

        return message;
    }

    private void resetBottomBarUI(){
        for(int i = 0 ; i<bottomNaviList.size() ; i++){
            BottomNaviHolder checkNavi = bottomNaviList.get(i);
            int checkCateType = checkNavi.cateType;
            if(clickedCateType == checkCateType){
                int color = ContextCompat.getColor(mContext, R.color.black);
                checkNavi.name.setTextColor(color);
            }else{
                int color = ContextCompat.getColor(mContext, R.color.white);
                checkNavi.name.setTextColor(color);
            }
        }
    }
}
