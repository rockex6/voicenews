package com.set.app.voicenews.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by kennethyeh on 2017/1/4.
 */

public class NumberToReadFormat {
    private static final NumberFormat numberFormat = new DecimalFormat("###,###,###");
    public synchronized static String getConvertFormat(int count){
        String formatCount = "";

        formatCount  = numberFormat.format(count);

        return formatCount;
    }
}

