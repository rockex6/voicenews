package com.set.app.voicenews.frag.cate;


import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.set.app.voicenews.MainActivity;
import com.set.app.voicenews.R;
import com.set.app.voicenews.constant.APIConstant;
import com.set.app.voicenews.constant.GlobalConstant;
import com.set.app.voicenews.dataInfo.DataInfo;
import com.set.app.voicenews.frag.search.FragSearchNewsResult;
import com.set.app.voicenews.requestcontrol.ApiRequestControl;
import com.set.app.voicenews.requestcontrol.RequestPara;
import com.set.app.voicenews.requestcontrol.RequestParseType;
import com.set.app.voicenews.util.DebugLog;
import com.set.app.voicenews.util.connect.ConnectionConstant;
import com.set.app.voicenews.util.prefs.SearchRecordUtil;
import com.set.app.voicenews.view.recyclerview.ViewTypeConstant;
import com.set.app.voicenews.view.recyclerview.ViewTypeInfo;
import com.set.app.voicenews.view.recyclerview.bindview.BVSearchNews;
import com.set.app.voicenews.view.recyclerview.createview.CVGeneralList;
import com.set.app.voicenews.view.recyclerview.typeconvert.CTSearchNews;

import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.util.List;

/**
 * Created by ResonTeng on 2017/1/9.
 */
public class FragCateSearchNews extends Fragment {
    private static final String TAG             = "FragCateSearchNews";
    public static final String FRAG_TRANS_NAME 	= "FragCateSearchNews";
    private static Context mContext             = null;

    private Menu mMenu;
    private Toolbar mToolbar;
    private SearchView mSearchView;
    private RecyclerView sRecyclerView;
    private static RecyclerViewAdapter sRecyclerViewAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private float viewHeight = 0;
    private LinearLayoutManager layoutManager;

    private ProgressBar mProgressBar;
    private boolean loadingFlag = false;
    private ApiRequestControl mApiRequestControl;
    private RelativeLayout mRefreshRL;
    private TextView mAlertTV;

    private String searchKeyword;
    private DataInfo.SearchHotDO mSearchHotDO;

    public static FragCateSearchNews newInstance() {
        FragCateSearchNews fragment = new FragCateSearchNews();
        return fragment;
    }

    public FragCateSearchNews() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

//    private View currentLayout=null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View nowLayout = null;
//        if(currentLayout == null){
            nowLayout = inflater.inflate(R.layout.frag_cate_search_news, null);

            setHasOptionsMenu(true);
            mToolbar = (Toolbar) nowLayout.findViewById(R.id.toolbar);
            ((AppCompatActivity) getActivity()).setSupportActionBar(mToolbar);
//            ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
//            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mToolbar.setTitle(getString(R.string.search_news_title));
            if (Build.VERSION.SDK_INT >= 21) {
                mToolbar.setPadding(0, GlobalConstant.Status_Bar_Height, 0, 0);
            }

            mProgressBar = (ProgressBar) nowLayout.findViewById(R.id.loadingProgressBar);
            mRefreshRL = (RelativeLayout) nowLayout.findViewById(R.id.refreshRL);
            mRefreshRL.setOnClickListener(retryRequestListener);
            mAlertTV = (TextView) nowLayout.findViewById(R.id.alertTV);

            sRecyclerView = (RecyclerView) nowLayout.findViewById(R.id.recyclerViewHistory);
            mSwipeRefreshLayout = (SwipeRefreshLayout) nowLayout.findViewById(R.id.swipeRefreshLayout);
            mSwipeRefreshLayout.setEnabled(false);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            sRecyclerView.setLayoutManager(linearLayoutManager);

            ViewTreeObserver vto = sRecyclerView.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    sRecyclerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    //viewWidth   = mSearchResultView.getMeasuredWidth();
                    viewHeight = sRecyclerView.getMeasuredHeight();
                    //DebugLog.d(TAG, "onGlobalLayout getWidt:"+formWidth);
                }
            });

//            currentLayout = nowLayout;
//        }else{
//            DebugLog.d(TAG, "nowLayout != null");
//            ViewGroup parent = (ViewGroup) currentLayout.getParent();
//            if (parent != null){
//                parent.removeView(currentLayout);
//            }
//            nowLayout = currentLayout;
//        }

        dataRequest();

        return nowLayout;
    }

    private void dataRequest(){
        if(loadingFlag){//資料截取中
            return;
        }
        RequestPara mRequrestPara = new RequestPara();
        mRequrestPara.httpMethod 	= RequestPara.HTTP_METHOD_GET;
        mRequrestPara.parseType   	= RequestParseType.ParseType.SearchNews;
        mRequrestPara.requestURL 	= getRequestURL();

        mApiRequestControl = new ApiRequestControl(requestCallBack, mRequrestPara, true);
        mApiRequestControl.startRequest();
        mProgressBar.setVisibility(View.VISIBLE);
        loadingFlag = true;
    }

    private String getRequestURL(){
        String url = "";
        url = String.format(APIConstant.API_SEARCH_HOT);
        return url;
    }

    private ApiRequestControl.CallBack requestCallBack = new ApiRequestControl.CallBack() {
        @Override
        public void doCallBack(Object obj) {
            RequestPara requrestPara = (RequestPara) obj;
            RequestParseType.ParseType parseType = requrestPara.parseType;

            switch (parseType) {
                case SearchNews:
                    DebugLog.d(TAG, "CateSearchNews!");

                    int httpStatus = requrestPara.httpStatus;
                    DebugLog.d(TAG, "httpStatus:" + httpStatus);
                    if(checkConnectError(httpStatus)){
                        String alertStr = "連線異常 請確認網路狀態請稍後再試！";
//                        Toast.makeText(mContext, "連線異常請稍後再試！", Toast.LENGTH_SHORT).show();
                        retryRequest(alertStr);
                        finishResponse();
                        return;
                    }
                    if(httpStatus!= HttpURLConnection.HTTP_OK){
                        String alertStr = "Server連線異常請稍後再試！";
//                        Toast.makeText(mContext, alertStr, Toast.LENGTH_SHORT).show();
                        retryRequest(alertStr);
                        finishResponse();
                        return;
                    }

                    mSearchHotDO = (DataInfo.SearchHotDO) requrestPara.responseObject;
                    DebugLog.d(TAG, (mSearchHotDO == null) + " null");

                    if(mSearchHotDO==null){
                        String alertStr = "資料解析異常請稍後再試！";
//                        Toast.makeText(mContext, alertStr, Toast.LENGTH_SHORT).show();
                        retryRequest(alertStr);
                        finishResponse();
                        return;
                    }

                    if (mSearchHotDO!=null){
                        SearchHistory(null, false);
                    }

                    finishResponse();
                    break;
            }
        }
    };

    private void finishResponse(){
        mProgressBar.setVisibility(View.GONE);
        loadingFlag = false;
        DebugLog.d(TAG, "-----------");
    }

    private boolean checkConnectError(int httpStatus){
        boolean flag = false;
        if(httpStatus == ConnectionConstant.ResError.CONNECT_TIMED_OUT
                || httpStatus == ConnectionConstant.ResError.CONNECT_ERROR
                ){
            flag = true;
        }
        return flag;
    }

    /**
     * list無法正常取得 retry
     * **/
    public void retryRequest(String alert){
        mAlertTV.setText(alert);
        mRefreshRL.setVisibility(View.VISIBLE);
    }
    private View.OnClickListener retryRequestListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            mRefreshRL.setVisibility(View.GONE);
            dataRequest();
            closeInputMethod();
        }
    };

    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private List<ViewTypeInfo> viewTypeList;
        private CVGeneralList mCViewSearchNews;
        private BVSearchNews mSViewSearchNews;

        public RecyclerViewAdapter(List<ViewTypeInfo> viewTypeList) {
            this.viewTypeList = viewTypeList;
            mCViewSearchNews  = CVGeneralList.getInstance();
            mSViewSearchNews  = new BVSearchNews(mContext, viewTypeList, mSearchView);
        }

        @Override
        public int getItemCount() {
            return viewTypeList.size();
        }

        @Override
        public int getItemViewType(int position) {
            return viewTypeList.get(position).viewType;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder = mCViewSearchNews.getViewHolder(parent, viewType);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
            mSViewSearchNews.setViewHolder(viewHolder, position);
        }
    }

    final private android.support.v7.widget.SearchView.OnQueryTextListener queryListener = new android.support.v7.widget.SearchView.OnQueryTextListener()
    {
        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }
        @Override
        public boolean onQueryTextSubmit(String query) {
            //DebugLog.d(TAG, "QueryTextSubmit: "+query);
            String queryText = query.trim();
            if (!queryText.equalsIgnoreCase("")) {
                searchKeyword = queryText;
                SearchHistory(searchKeyword, true);

                //搜尋結果頁
                closeInputMethod();
//                FragSearchNewsResult fragment = FragSearchNewsResult.newInstance(searchKeyword);
//                ((MainActivity) mContext).mFragRepTransUtil.replaceContentFragment(fragment, fragment.FRAG_TRANS_NAME);

                ((MainActivity) mContext).removeCateFrag();// 移除上一層toolbar含searchview Fragment
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        FragSearchNewsResult fragment = FragSearchNewsResult.newInstance(searchKeyword);
                        ((MainActivity) mContext).mFragRepTransUtil.replaceContentFragment(fragment, fragment.FRAG_TRANS_NAME);
                    }
                }, 150);

            } else {
                Toast.makeText(mContext, "請輸入關鍵字！", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        mMenu = mToolbar.getMenu();
        mMenu.clear();
        inflater.inflate(R.menu.menu_cate_search_news, mMenu);
        final MenuItem item = mMenu.findItem(R.id.action_search_news_list);
        mSearchView = (SearchView) MenuItemCompat.getActionView(item);
        mSearchView.setQueryHint(getString(R.string.search_news_title)+"...");
        mSearchView.setIconified(false);
        mSearchView.onActionViewExpanded();
        mSearchView.setOnQueryTextListener(queryListener);

        mSearchView.clearFocus();

        mToolbar.setNavigationIcon(R.mipmap.ic_search_w);
        mSearchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DebugLog.d(TAG, "onClick");
                mToolbar.setNavigationIcon(R.mipmap.ic_search_w);
            }
        });
        mSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                DebugLog.d(TAG, "onClose");
                mToolbar.setNavigationIcon(null);
                return false;
            }
        });

        mSearchView.requestFocus(); //只顯示光標Cursor

        SearchView.SearchAutoComplete searchTextView = (SearchView.SearchAutoComplete)mSearchView.findViewById(R.id.search_src_text);
//        theTextArea.setTextColor(Color.WHITE); //set any color that you want
//        theTextArea.setHint(getString(R.string.search_news_title)+"..."); //QueryHint文字
        try { //自訂光標Cursor
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            //This sets the cursor resource ID to 0 or @null which will make it visible on white background
            mCursorDrawableRes.set(searchTextView, R.drawable.color_cursor);
        } catch (Exception e) {}

        super.onCreateOptionsMenu(mMenu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search_news_list) {
            DebugLog.d(TAG, "SearchNews");
            return true;
        }

        if (id == android.R.id.home){
//            // get focus
//            mSearchView.requestFocus();
//            // get soft input
//            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
            return true;
        }

        return super.onOptionsItemSelected(item); // important line
    }

    private void closeInputMethod(){
        mSearchView.clearFocus();
        try {
            InputMethodManager input = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
            input.hideSoftInputFromWindow(((Activity)mContext).getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {}
    }

    //搜尋歷史記錄
    public void SearchHistory(String keyword, boolean isSerarch){
        SearchRecordUtil mSearchRecordUtil = SearchRecordUtil.getInstance(mContext);
        List<String> keywords = mSearchRecordUtil.getRecordsList();

        if(isSerarch){ //紀錄搜尋關鍵字
            keywords = mSearchRecordUtil.putKeyword2RecordsList(keyword, keywords);
        }

        if(mSearchHotDO!=null){
            CTSearchNews mCTypeSearchNews = new CTSearchNews(keywords, mSearchHotDO);
            List<ViewTypeInfo> mViewTypeList;
            mViewTypeList = mCTypeSearchNews.convertType();
            sRecyclerViewAdapter = new RecyclerViewAdapter(mViewTypeList);
            sRecyclerView.setAdapter(sRecyclerViewAdapter);
        }
    }

    public static void removeRecord(String name, String nextRecord){
        for(int i=0; i<sRecyclerViewAdapter.viewTypeList.size(); i++){
            if(sRecyclerViewAdapter.viewTypeList.get(i).viewType == ViewTypeConstant.GeneralCellType.ITEM_SEARCH_RECORD_CONTENT){
                DataInfo.SearchRecordDO mSearchRecordDO = (DataInfo.SearchRecordDO)sRecyclerViewAdapter.viewTypeList.get(i).dataObject;
                if(mSearchRecordDO.showRemoveIcon && name.equalsIgnoreCase(mSearchRecordDO.keyword)){
                    sRecyclerViewAdapter.viewTypeList.remove(i);
                    sRecyclerViewAdapter.notifyItemRemoved(i);

                    if(nextRecord!=null){ //若有下一筆紀錄則新增顯示
                        DataInfo.SearchRecordDO nextSearchRecordDO = new DataInfo.SearchRecordDO();
                        nextSearchRecordDO.showRemoveIcon = true;
                        nextSearchRecordDO.keyword = nextRecord;
                        ViewTypeInfo mViewTypeInfo = new ViewTypeInfo();
                        mViewTypeInfo.viewType   = ViewTypeConstant.GeneralCellType.ITEM_SEARCH_RECORD_CONTENT;
                        mViewTypeInfo.dataObject = nextSearchRecordDO;
                        int lastId = sRecyclerViewAdapter.viewTypeList.size();
                        sRecyclerViewAdapter.viewTypeList.add(lastId, mViewTypeInfo);
                        sRecyclerViewAdapter.notifyItemInserted(lastId);
                    }
                    return;
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(mApiRequestControl!=null){
            mApiRequestControl.cancelTask();
        }

        closeInputMethod();
    }
}
