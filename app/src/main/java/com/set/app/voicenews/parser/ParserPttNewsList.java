package com.set.app.voicenews.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import com.set.app.voicenews.dataInfo.DataInfo;

import org.json.JSONException;

import java.io.StringReader;

/**
 * Created by kennethyeh on 2017/1/3.
 */

public class ParserPttNewsList {

    public DataInfo.PttNewsList parsing(String value) throws JSONException {

        DataInfo.PttNewsList mPttNewsList = null;

        StringReader jsonSR  = new StringReader(value);

        GsonBuilder gsonb = new GsonBuilder();
        Gson gson = gsonb.create();

        JsonReader reader = new JsonReader(jsonSR);
        reader.setLenient(true);
//        Type collectionType = new TypeToken<CateListMix>(){}.getType();
        mPttNewsList = gson.fromJson(reader, DataInfo.PttNewsList.class);

        return mPttNewsList;
    }

}
