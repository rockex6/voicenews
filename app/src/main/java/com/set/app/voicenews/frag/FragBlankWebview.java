package com.set.app.voicenews.frag;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.set.app.voicenews.R;
import com.set.app.voicenews.constant.APIConstant;
import com.set.app.voicenews.util.DebugLog;

/**
 * Created by kennethyeh on 2017/1/20.
 */

public class FragBlankWebview extends Fragment {

    private static final String TAG             = "FragBlankWebview";
    public String FRAG_TRANS_NAME 	            = "FragBlankWebview";
    private Context mContext                    = null;
    private ProgressBar mProgressBar;
    private WebView wvWeb = null;
    private boolean useViewPort = true;

    private RelativeLayout mRefreshRL;
    private TextView mAlertTV;

    public static final String Para_key_URL     = "url" ;
    private String url   = "";

    public static FragBlankWebview newInstance(String url) {
        FragBlankWebview frag = new FragBlankWebview();
        Bundle b = new Bundle();
        b.putString(Para_key_URL, url);
        frag.setArguments(b);
        return frag;
    }

    public FragBlankWebview(){}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext    = context;
        url         = getArguments().getString(Para_key_URL,"");
//        DebugLog.d(TAG, "FRAG_TRANS_NAME:" + FRAG_TRANS_NAME);
    }

    private View currentLayout=null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View nowLayout = null;

        nowLayout  	    = inflater.inflate(R.layout.frag_blank_webview, null);
        mProgressBar = (ProgressBar) nowLayout.findViewById(R.id.progressBar);
        mProgressBar.setMax(100);
        mProgressBar.setVisibility(View.INVISIBLE);

        mRefreshRL      = (RelativeLayout) nowLayout.findViewById(R.id.refreshRL);
        mRefreshRL.setOnClickListener(retryRequestListener);
        mAlertTV        = (TextView) nowLayout.findViewById(R.id.alertTV);
        mRefreshRL.setVisibility(View.GONE);

        wvWeb = (WebView) nowLayout.findViewById(R.id.wvWeb);

        if (Build.VERSION.SDK_INT >= 21) {
            wvWeb.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            CookieManager.getInstance().setAcceptThirdPartyCookies(wvWeb, true);
        }

        wvWeb.getSettings().setJavaScriptEnabled(true);
        if(useViewPort){
            wvWeb.getSettings().setUseWideViewPort(true);
            wvWeb.getSettings().setLoadWithOverviewMode(true);
        }

        wvWeb.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                mProgressBar.setProgress(0);

                if(url.indexOf("news://")>-1){ //ETtoday News
                    int indexStart = url.indexOf("/", 7)+1;
                    int indexEnd = url.indexOf("/", indexStart);
                    String ettodayNewsId = url.substring(indexStart, indexEnd);
                    url = String.format(APIConstant.ETtoday_News_API, ettodayNewsId);
//                    DebugLog.d(TAG, "ettoday url: "+url);
                }

                view.loadUrl(url);
                mProgressBar.setVisibility(View.VISIBLE);
                return true;
            }
            @Override
            public void onPageFinished(WebView view, String url) {
//                DebugLog.i(TAG, "onPageFinished url:"+url);
                if(url.startsWith("https://m.facebook.com/plugins/close_popup.php")){
//                    DebugLog.d(TAG, "startsWith url:"+url);
                    return;
                }
                super.onPageFinished(view, url);
            }
        });

        wvWeb.setWebChromeClient(chromeClient);
        DebugLog.d(TAG, "url:"+url);
        mProgressBar.setProgress(0);
        wvWeb.loadUrl(url);
        mProgressBar.setVisibility(View.VISIBLE);
//        loadWebview();
//        trackGAView();
        return nowLayout;
    }

    private WebChromeClient chromeClient = new WebChromeClient() {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            setValue(newProgress);
            super.onProgressChanged(view, newProgress);
        }
    };

    public void setValue(int progress) {
//		Log.d(TAG, "progress:" + progress);
        mProgressBar.setProgress(progress);
        if(progress==100){
            mProgressBar.setVisibility(View.INVISIBLE);
            mProgressBar.setProgress(0);
        }
    }

    private void loadWebview(){
        mProgressBar.setProgress(0);
        wvWeb.loadUrl(url);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private View.OnClickListener retryRequestListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            mRefreshRL.setVisibility(View.GONE);
            loadWebview();
        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        if(wvWeb!=null){
            DebugLog.d(TAG, "onDestroy");
            wvWeb.onPause();
            wvWeb.destroy();
            wvWeb = null;
        }
    }

    public void onBackPressed() {
        if (wvWeb.canGoBack()) {
            DebugLog.d(TAG,"canGoBack");
            wvWeb.goBack();
        } else {
            ((Activity)mContext).finish();
        }
    }
}
